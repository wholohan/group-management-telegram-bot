async function xReq(obj)
{	if (obj.data == undefined) obj.data = {};
	obj.data.license_id = license_id;
	body = JSON.stringify(obj);
	var promise = await fetch("/bot/?ajax=1", {
		method: 'POST',
		headers: {
   		'Content-Type': 'application/json;charset=utf-8'
  		},
  		body: body
	});
	return obj = promise.json();
}

function toggleDrawer()
{	var drawer = document.getElementById('right-pane');
	drawer.classList.toggle('open-drawer');
}

function clReturn(event)
{	if (event.key == 'Enter' && event.shiftKey == false) clRun();
	else if(event.target.value.includes("search"))
	{	var parts = event.target.value.split(' ');
		if (parts[2] != undefined && parts[2].length > 3 ) clRunNoClear();
	}
}

function clFill(fill,wait)
{	var cl = document.getElementById('cl');
	var drawer = document.getElementById('right-pane');
	cl.value = fill;
	if (drawer.classList.contains('open-drawer') == true) toggleDrawer(); 
	if (wait != "true")
	{	cl.classList.add('filled');
		window.setTimeout(()=>{clRun();cl.classList.remove('filled');},500);
	}	else 
	{	cl.focus();
	}
}

function clRun()
{	var cl = document.getElementById('cl');
	var cmd = cl.value.split(' ',1);
	var trail = cl.value.substr(cmd[0].length+1); 
	cl.value = '';
	cl.blur();
	cl.classList.remove('filled');
	window[cmd[0]](trail);
}

function clRunNoClear()
{	var cl = document.getElementById('cl');
	var cmd = cl.value.split(' ',1);
	var trail = cl.value.substr(cmd[0].length+1); 
	window[cmd[0]](trail);
}

function thinking()
{	document.getElementById('cl-button-row').classList.toggle("thinking");
}

function commitOG(elem)
{	var obj = elem.closest("data-div").obj;
	xReq({classname:"LicenseUser",method:"edit",data:{id:obj.lu_id,og:(elem.checked === true ? 1 : 0)}});
}

function commitFem(elem)
{	var obj = elem.closest("data-div").obj;
	xReq({classname:"LicenseUser",method:"edit",data:{id:obj.lu_id,fem:(elem.checked === true ? 1 : 0)}});
}

function commitColored(elem)
{	var obj = elem.closest("data-div").obj;
	xReq({classname:"LicenseUser",method:"edit",data:{id:obj.lu_id,colored:(elem.checked === true ? 1 : 0)}});
}

function commitUI(elem)
{	var obj = elem.closest("data-div").obj;
	xReq({classname:"LicenseUser",method:"edit",data:{id:obj.lu_id,can_ui:(elem.checked === true ? 1 : 0)}});
}

function commitAdmin(elem)
{	var obj = elem.closest("data-div").obj;
	xReq({classname:"LicenseUser",method:"edit",data:{id:obj.lu_id,admin_id:((elem.value == "null") ? null : elem.value)}});
}

function commitProtection(elem)
{	var obj = elem.closest("data-div").obj;
	xReq({classname:"LicenseUser",method:"edit",data:{id:obj.lu_id,protection_id:((elem.value == "null") ? null : elem.value)}});
}

function commitNotes(elem)
{	var obj = elem.closest("data-div").obj;
	xReq({classname:"Action",method:"edit",data:{id:obj.id,notes:elem.value}});
}

function commitEnabled(elem)
{	var obj = elem.closest("data-div").obj;
	xReq({classname:"Config",method:"edit",data:{id:obj.id,enabled:(elem.checked === true ? 1 : 0)}});
}

function commitDescription(elem)
{	var obj = elem.closest("data-div").obj;
	xReq({classname:"Config",method:"edit",data:{id:obj.id,description:elem.value}});
}

function changeLicense(event)
{	window.open('/bot/?license_id=' + event.target.value,'_self');
}

//________________________________________________________________________________________________________

function users(trail)
{	if (trail === undefined) trail = "dynamic";
	var box = document.querySelector("response-body");
	if (trail == "dynamic") {
		box.multiQuery({'classname':'User','method':trail},"userRowHTML");
	}	else if (trail.startsWith("search"))
	{	var toSearch = trail.substr(7);
		box.query({'classname':'User','method':'search','data':{'toSearch':toSearch}},"userRowHTML");	
	}	else 
	{	box.query({'classname':'User','method':trail},"userRowHTML");
	}
}

function log(trail)
{	var box = document.querySelector("response-body");
	var obj = {"classname":"Action","method":"a_log"};
	if (trail != undefined) obj.data = {"type":trail};
	box.query(obj,"logRowHTML");
}

function configs(trail)
{	var box = document.querySelector("response-body");
	box.query({'classname':'Config','method':'all'},"configRowHTML");
}

//________________________________________________________________________________________________________

function userRowHTML(obj,elem)
{	var elem = (elem !== undefined) ? elem : document.querySelector(".cache .user-row").cloneNode(true);
	elem.querySelector(".last-name").innerHTML = obj.last_name; 	
	elem.querySelector(".first-name").innerHTML = obj.first_name; 	
	if(obj.username != null) 
	{	elem.querySelector(".username").innerHTML = '@' + obj.username;
	}
	elem.querySelector(".og").checked = (obj.og == 1) ? true : false; 	
	elem.querySelector(".ui").checked = (obj.can_ui == 1) ? true : false;
	elem.querySelector(".fem").checked = (obj.fem == 1) ? true : false;
	elem.querySelector(".colored").checked = (obj.colored == 1) ? true : false;
	elem.querySelector(".admin").value = obj.admin_id;
	elem.querySelector(".protection").value = obj.protection_id;
	return elem;  	
}

function logRowHTML(obj,elem)
{	var elem = (elem !== undefined) ? elem : document.querySelector(".cache .log-row").cloneNode(true);
	elem.querySelector(".timestamp").innerHTML = (new Date(obj.timestamp).toUTCString()); 	
	if(obj.until == null)
	{	elem.querySelector(".dates").removeChild(elem.querySelector(".until").closest(".lite"));
	}	else
	{	elem.querySelector(".until").innerHTML = (new Date(obj.until).toUTCString());
	} 	
	elem.querySelector(".type").innerHTML = obj.type; 	
	elem.querySelector(".affected-user").innerHTML = (obj.affected_first ? obj.affected_first :'') + ' ' + (obj.affected_last ? obj.affected_last :'');
	elem.querySelector(".acting-user").innerHTML = (obj.acting_first ? obj.acting_first :'') + ' ' + (obj.acting_last ? obj.acting_last :'');
	elem.querySelector(".chat").innerHTML = obj.chat; 	
	elem.querySelector(".notes").value = obj.notes;
	elem.querySelector(".link").href = 'https://t.me/c/' + obj.chatid.toString().substr(4) + '/' + obj.messageid.split('_')[1];
	return elem;  	
}

function configRowHTML(obj,elem)
{	var elem = (elem !== undefined) ? elem : document.querySelector(".cache .config-row").cloneNode(true);
	elem.querySelector(".name").innerHTML = obj.name; 	
	elem.querySelector(".description").value = obj.description;
	elem.querySelector(".enabled").checked = (obj.enabled == 1) ? true : false;
	return elem;  	
}


//________________________________________________________________________________________________________

class FillList extends HTMLElement
{	constructor()
	{	super();
		var fills = this.getAttribute('data-fills').split(',');
		for (var i=0;i<fills.length;i++)
		{	var fill = fills[i];
			var bubble = document.createElement('span');
			bubble.classList.add('fill');
			if (fill.substr(0,1) == "!")
			{	fill = fill.substr(1);
				bubble.setAttribute("data-wait",true);
			}
			bubble.innerHTML = fill;
			bubble.onclick = (e) => {clFill(e.target.innerHTML,e.target.getAttribute("data-wait"))};
			this.appendChild(bubble);
		}
	}
}

class DataDiv extends HTMLElement
{	constructor()
	{	super();
	}
	static get observedAttributes() { return ['id','classname']; }
	update()
	{	var id = this.getAttribute('classname');	
		var classname = this.getAttribute('classname');	
	}
}

class ResponseBody extends HTMLElement
{	constructor()
	{	super();
	}
	static get observedAttributes() { return ["build"]; }

	query(data,build) {
		thinking();		
		this.innerHTML = '';
		this.classList.add("hidden");
		this.setAttribute('build',build);
		xReq(data).then(response => {
			this.innerHTML = '';
			this.populate(response.data);
			thinking();
		});
	}
	
	async	multiQuery(data,build) {
		thinking();
		this.innerHTML = '';
		this.classList.add("hidden");
		this.setAttribute('build',build);
		for(var i = 0;i<40;i++)
		{	data.data = {};
			data.data.offset = i*200;
			console.log(i);
			var response = await xReq(data);
			if(response.data != null) {
				this.populate(response.data);
			}	else {
				thinking();
				break;
			}
		}
	}

	populate(set) {
		this.classList.remove("hidden");
		var build = this.getAttribute("build");
		if (set == null) return;
		for(var i=0;i<set.length;i++)
		{	var obj = set[i];
			var elem = window[build](obj);
			elem.setAttribute("builtas",build);
			elem.obj = obj;
			this.appendChild(elem);
		}
	}
}

customElements.define('fill-list',FillList);
customElements.define('data-div',DataDiv);
customElements.define('response-body',ResponseBody);