<?php
$db_user = (new User($conn,'ezread'))->run(['field' => 't_id', 'value' => $_SESSION['user']['id'] ]);
$licenses = (new LicenseUser($conn,'can_ui'))->run(['user_id' => $db_user['id']]);
$_SESSION['license'] = $licenses[$_GET['license_id']] ?: $licenses[array_key_first($licenses)];
if($_SESSION['license'])
{	$licensed_user = (new User($conn,'read',$db_user['id']))->run(['license_id' => $_SESSION['license']['id'] ]);
}

?>

<!DOCTYPE html>
<html>
	<head>
		<title>
			Bot Admin Portal		
		</title>
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<style>
			body { background-color: whitesmoke;display:flex; flex-direction:column;position:relative; }
			#fuggoff { background-color: whitesmoke;display:flex; justify-content: flex-end; margin-top: 2em; font-family:monospace; color: black; }
			#main { background-color: whitesmoke;display:flex; justify-content: space-between; }
			#top { background-color: whitesmoke;display:flex; justify-content: space-between; align-items: baseline;}
			#logged-in-as { display: flex; align-items: center; font-family:monospace; color: black;}
			#user-and-chat { display: flex; flex-direction: column; }
			#user-and-chat select { border-radius: 2px; background-color: white; }
			#pfp { border-radius: 50%; margin-left: 10px; };
			#main { background-color: whitesmoke;display:flex; justify-content: space-between; }
			h1 { text-align: center; font-family:monospace; color: black; font-size:1.5em; }
			#cl { border: 1px solid black; padding: 1em; background-color: white; border-radius: 5px; resize:none; font-family: monospace; }		
			#cl-button { border-radius: 5px; background-color: whitesmoke; color: black; border: none; font-size: 1.5em; cursor: pointer; margin: 0.25em 0; }
			#cl-shell { display: flex; flex-direction: column;}		
			#cl-button-row { display: flex; flex-direction: row; justify-content: start; }
			#cl-button-row.thinking button { display: none;}
			#cl-button-row img { display: none; height: 128px; width: auto; margin: -40px 0 0 0;}
			#cl-button-row.thinking img {display: inline;}
			#right-pane {display: inline-flex; flex-direction:column; width:15%; flex-wrap: wrap;}
			#quick {display: inline-flex; flex-direction:column; flex-wrap: wrap;}
			#left-pane {width:78%; display:inline-block; margin-left: 3em; }
			#drawer{display:none;font-size: 1.5em;}
			response-body {display: flex; flex-direction: column; border: 1px inset black; border-radius: 5px; padding: 0.5em; overflow: auto; /*max-height: 50vh;*/}
			.row { display: flex; background-color: white; border-bottom: 1px solid lightgrey; padding: 0.4em 1em; border-radius: 3px; align-items: baseline;}
			.row:nth-of-type(2n) { background-color: whitesmoke;}
			.user-row { justify-content: space-between; }
			.user-row .username { opacity: 0.7; font-size: 0.8em; }
			.user-toggles { display: inline-flex; flex-wrap:wrap; justify-content: end;}
			.user-checks { display: inline-flex; flex-wrap:wrap; justify-content: end;}
			.log-row { flex-direction: column; align-items: stretch;}
			.log-row > span { border-bottom: 1px dotted lightgrey; display: flex; padding: 0.3em; align-items: stretch; justify-content: space-between;}
			.lite { opacity: 0.7; font-size: 0.8em;}
			.log-details > span { align-items: baseline !important;}
			.notes { width: 100%; height: 100%; resize: none;}
			.config-row { align-items: flex-end;}
			.fill {background-color: #ff8000; border: 1px solid black; border-radius: 5px; padding: 0.2em 0.3em; margin: 0.25em;cursor: pointer; user-select:none}
			.fill:hover { box-shadow: 0 0 2px 2px rgba(255, 128, 0, 0.7), 0 2px 2px 0 rgba(255, 128, 0, 0.69); border-style: outset;}
			.filled {background-color: #ff8000 !important; border: 1px solid black; box-shadow: 0 0 2px 2px rgba(255, 128, 0, 0.7), 0 2px 2px 0 rgba(255, 128, 0, 0.69);}
			.cache {display: none;}
			.hidden {visibility: hidden;}
			.gone {display: none;}
			a:link, a:visited, a:hover, a:active { color: black;}
			
			@media screen and (max-width: 800px)
			{	#right-pane	{display:none;position:absolute;left:0;width: 100%;height: 100vh;background-color: whitesmoke;}
				#left-pane {width:100%;margin:0}
				#drawer{display:inline;}
				.open-drawer{display:flex !important;}
				.row{flex-direction:column; border-radius: 0px; text-align:center;}
				response-body{border: 0px;padding:0.5em 0em;border-radius:0px;}
				.log-row > span {flex-direction: column;}
				.user-row > span {margin: 0.3em 0;}
				.user-row > span:first-child {border-bottom: 1px dotted lightgrey;}
				#cl-button-row {justify-content: center;}
				.config-row {align-items: flex-start;}
				.config-row input {max-width: 85vw; }
			} 
		</style>
	</head>
	<body>
		<section id="top">
			<span id="drawer" onclick="toggleDrawer()">...</span>		
			<span id="logged-in-as">
				<span id="user-and-chat">
					<span><?php echo $_SESSION['user']['first_name'].' '.$_SESSION['user']['last_name']; ?></span>
					<select onchange="changeLicense(event)">

<?php 
if(@$licenses)
{	foreach ($licenses as $row)
	{	
?>

						<option <?php echo $row['license_id'] == $_SESSION['license']['license_id'] ? 'selected="selected"':'' ;?> value="<?php echo $row['license_id']; ?>">
							<?php echo $row['name'];?>
						</option>

<?php
	}
}
?>				

					</select>
				</span>
				<img id="pfp" src="<?php echo $_SESSION['user']['photo_url']; ?>" height="40px" width="40px" alt="you"/>
			</span>		
		</section>
<?php if(!$licensed_user || 0/*!$licensed_user['can_ui']*/)
{
?>

		<section id="fuggoff">
			You may not use the toy.		
		</section>

<?php
}	else
{
?>
		<section id="main">
			<section id="left-pane">
				<h1>Admin Portal</h1>
				<section id="cl-shell">
					<textarea id='cl' rows="4" onkeydown="clReturn(event)"></textarea>
					<span id="cl-button-row">
						<button id="cl-button" onclick="clRun()" >>></button>
						<img src="librsc/wheel.gif" alt="loading">
					</span>
				</section>
				<response-body class="hidden">
				</response-body>
			</section>
			<section id="right-pane">
				<h1>
					Quick
				</h1>
				<fill-list id="quick" data-fills="configs,!users search ,users admin,users fem,users og,log,log ban,log mute,log gag,log warn,stats"
				>
				</fill-list>
			</section>
			<section class="cache">
				<data-div class="row user-row">
					<span>
						<span class="first-name">
						</span>
						<span class="last-name">
						</span>
						<span class="username">
						</span>
					</span>
					<span class="user-checks">
						og:
						<input class="og" type="checkbox" onchange="commitOG(this)"/>
						&nbsp;
						fem:
						<input class="fem" type="checkbox" onchange="commitFem(this)"/>
						&nbsp;
						colored:
						<input class="colored" type="checkbox" onchange="commitColored(this)"/>
						&nbsp;
						ui:
						<input class="ui" type="checkbox" onchange="commitUI(this)"/>
					</span>
					<span class="user-toggles">
						admin:					
						&nbsp;
						<select class="admin" onchange="commitAdmin(this)">
							<option value="null"></option>
							<option value="2">bottom</option>					
							<option value="3">versatile</option>					
							<option value="4">top</option>					
						</select>
						&nbsp;
						donor:					
						&nbsp;
						<select class="protection" onchange="commitProtection(this)">
							<option value="null"></option>
							<option value="2">contributor</option>					
							<option value="1">donor</option>					
						</select>
					</span>					
				</data-div>		
				<data-div class="row log-row">
					<span class="log-header">
						<span class="type"></span>
						<span class="dates">
							<span class="timestamp lite"></span>
							<span class="lite"> &#x2192; <span class="until"></span></span>
						</span>
					</span>
					<span class="log-details">
						<span>
							<span class="affected-user"></span>&nbsp;&nbsp;&#x2014;&nbsp;&nbsp;
							<span class="lite">by <span class="acting-user"></span></span>&nbsp;
							<span class="lite">in <span class="chat"></span></span>
						</span>
						<a class="link" target="_blank">&#x1F517;</a>
					</span>
					<span class="log-notes">
						<textarea class="notes" onblur="commitNotes(this)">
						</textarea>
					</span>
				</data-div>		
				<data-div class="row config-row">
					<span>
						<input type="checkbox" class="enabled"/ onchange="commitEnabled(this)">
						<span class="name"></span>:&nbsp;
					</span>
					<span>
						<input class="description" size="50" maxlength="50"/ onblur="commitDescription(this)">
					</span>
				</data-div>		
			</section>
		</section>

<?php
}
?>

	</body>
	<script type="text/javascript" src="/bot/librsc/admin_panel.js">
	</script>
	<script>
		var license_id = <?php echo $_SESSION['license']['license_id']; ?>;	
	</script>
</html>

