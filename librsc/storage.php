<?php
	/*
		Storage class is for SQL operations. 
		SQL never lives anywhere but here.
		Instantiate the class with the open PDO connection and the name of one of its protected methods,
		then call this->run to execute
		
		Invocation parameters:
			conn: MyPDO Object, the open database connection,
			method: the name of one of the protected methods in the class, to be called on this->run()

		Run parameters:
			arr: an array to pass into this->method on this->run

		Return:
			mixed, either a value or an array			 
	*/
	
	class Storage
	{	public function __construct(MyPDO $conn,$method = NULL,$id = NULL)
		{	$this->conn = $conn;
			if($method) $this->testMethod($method);
			if($id) $this->testId($id);
		}
		public function run($arr = [])
		{	$in_trans = $this->conn->inTransaction();
			if(!$in_trans) $this->conn->beginTransaction();
			try
			{	if(@$this->e_on_run) throw new Exception($this->e_on_run);
				$result = $this->{$this->method}($arr);
				if(!$in_trans) $this->conn->commit();
				return $result;
			}	catch (PDOException $e)
			{	$error_code = get_class($this).'|'.$this->method.'|'.$e->getMessage();
				if($in_trans) throw new Exception($error_code);
				$this->err_code = get_class($this).'|'.$this->method.'|'.$e->getMessage();
				$file = fopen($_SERVER['DOCUMENT_ROOT'].'/lastPDOerror.cache','wb');
				fwrite($file,json_encode(error_get_last()));
				fclose($file);
				$this->conn->rollback();
			}	catch (Exception $e)
			{	$error_code = get_class($this).'|'.$this->method.'|'.$e->getMessage();
				if($in_trans) throw new Exception($error_code);
				$this->err_code = get_class($this).'|'.$this->method.'|'.$e->getMessage();
				$this->conn->rollback();
			}	catch (Error $e)
			{	$error_code = get_class($this).'|'.$this->method.'|'.$e->getMessage();
				if($in_trans) throw new Exception($error_code);
				$this->err_code = get_class($this).'|'.$this->method.'|'.$e->getMessage();
				$this->conn->rollback();
			}
		}
		
		protected function testId($id)
		{	if(!($this->conn->run(" SELECT COUNT(*) FROM ".static::table." WHERE id=?",[$id])->fetch(PDO::FETCH_COLUMN))) 
			{	$this->e_on_run = "id $id does not exist in ".static::table;
			}	else
			{	$this->id = $id;
			}
		}
		protected function testMethod($method)
		{	if(!$method || !method_exists($this,$method)) 
			{	$this->e_on_run = 'Unrecognized method '.$method;
			}	else 
			{	$this->method = $method;
			}
		}		

		protected function create($data)
		{	foreach (static::fields as $field)
			{	if(@$data[$field]) $to_create[$field] = $data[$field];
			}
			$sql = " INSERT INTO ".static::table." (".implode(',',array_keys($to_create)).")
					VALUES (?".str_repeat(',?',(count($to_create)-1)).")";
			$this->conn->run($sql,array_values($to_create));
			$this->id = $this->conn->lastInsertId();
			return $this->read();
		}

		protected function edit($data)
		{	if(!$this->id) throw new Exception('Missing id for edit.');
			foreach (static::fields as $field)
			{	if(array_key_exists($field,$data)) $to_edit[$field] = $data[$field];
			}
			$sql = " UPDATE ".static::table."
					SET ".implode('=?,',array_keys($to_edit))."=?
					WHERE id=".$this->id;
			$this->conn->run($sql,array_values($to_edit));
			return $this->read();
		}
		
		protected function store($data)
		{	if(!defined('static::store')) throw new Exception('Class missing store constant.');
			foreach (static::fields as $field)
			{	if(@$data[$field]) $to_store[$field] = $data[$field];
				elseif(@$data['obj'][$field]) $to_store[$field] = $data['obj'][$field];
				elseif(static::store[$field] && @$data['obj'][static::store[$field]]) $to_store[$field] = $data['obj'][static::store[$field]];
			}
			$arr = [];
			if(!$to_store) return;
			foreach ($to_store as $key=>$value)
			{	$arr[] = "$key = VALUES($key)";
			}
			$sql = " INSERT INTO ".static::table." (".implode(',',array_keys($to_store)).")
					VALUES (?".str_repeat(',?',(count($to_store)-1)).") 
					ON DUPLICATE KEY UPDATE ".implode(', ', $arr);
			$this->conn->run($sql,array_values($to_store));
			$this->id = $this->ezid(['field' => 't_id','value' => $to_store['t_id']]);
			return $this->read();
		}
		
		protected function all($data)
		{	$sql = " SELECT *
					FROM ".static::table;
			return $this->conn->run($sql)->fetchAll();
		}

		protected function read($data = [])
		{	if(!$this->id) throw new Exception('Missing id for read.');
			if(defined('static::join'))
			{	foreach (static::join as $key => $row)
				{	$ins .= "LEFT JOIN $key
							ON a.".$row[0]." = $key.id";
					foreach ($row[1] as $field)
					{	$select .= ", $key.$field";
					}
				}
			}
			$sql = " SELECT a.*$select
					FROM ".static::table." as a
					$ins
					WHERE a.id=? ";
			return $this->conn->run($sql,[$this->id])->fetch();
		}
		
		protected function ezread($data)
		{	$this->id = $this->ezid($data);
			unset($data['field']);
			unset($data['value']);
			return $this->read($data);
		}
		
		protected function ezid($data)
		{	if(!defined('static::ez')) throw new Exception('Class not set up for ezid.');
			if($data['field'])
			{	if(!in_array($data['field'],static::ez)) throw new Exception('Column '.$data['field'].' not ez.');
				$field = $data['field'];
			}	else
			{	$field = static::ez[0];
			} 
			$sql = " SELECT id
					FROM ".static::table."
					WHERE $field=?";
			return $this->conn->run($sql,[$data['value']])->fetch(PDO::FETCH_COLUMN);
		}
		
		protected function search($data)
		{	if(!defined('static::search')) throw new Exception("Table '".static::table."' not set up for search.");
			$search = stripslashes($data['toSearch']);
			$search = htmlspecialchars($search);
			$sql = "SELECT * 
					FROM ".static::table."
					WHERE	MATCH (".implode(',',static::search).")
					AGAINST('$search*' IN BOOLEAN MODE)";
			return $this->conn->run($sql,[/*$data['toSearch']*/])->fetchAll();
		}
	}
	
	class Admin extends Storage
	{	const table = 'admins';
		const fields = ['name'];
		const ez = ['name'];
	}

	class Action extends Storage
	{	const table = 'actions';
		const fields = ['atype_id','affected_user','acting_user','until','code','notes','cancelled','chat_id','message_id'];
		const ez = ['name'];
		const join = [
			'atypes' => ['atype_id',['name as type']]
		];
		public function run($data = [])
		{	if(@$data['type'])
			{	$data['atype_id'] = (new Atype($this->conn,'ezid'))->run(['field' => 'name','value' => $data['type']]);
			}
			return parent::run($data);
		}
		protected function a_log($data)
		{	$where = @$data['atype_id'] ? "WHERE a.atype_id = ?" : ''; 
			$sql = " SELECT a.*, act.first_name as acting_first, act.last_name as acting_last, aff.first_name as affected_first, 
								aff.last_name as affected_last, at.name as type, c.name as chat, c.t_id as chatid, m.t_id as messageid
					FROM ".static::table." as a
					LEFT JOIN users as act
						ON a.acting_user=act.id
					LEFT JOIN users as aff
						ON a.affected_user=aff.id
					LEFT JOIN atypes as at
						ON a.atype_id=at.id
					LEFT JOIN chats as c
						ON a.chat_id=c.id
					LEFT JOIN messages as m
						ON a.message_id = m.id
					$where
					ORDER BY a.timestamp DESC ";
			$arr = @$data['atype_id'] ? [$data['atype_id']] : NULL;
			return $this->conn->run($sql,$arr)->fetchAll();
		}

		protected function active($data)
		{	$sql = "	SELECT *
					FROM ".static::table."
					WHERE affected_user=?
						AND atype_id=?
						AND (until IS NULL OR until > NOW())
						AND cancelled = 0
						AND chat_id =?
					ORDER BY timestamp DESC";
			return $this->conn->run($sql,[$data['affected_user'],$data['atype_id'],$data['chat_id']])->fetchAll();
		}
		protected function until($data)
		{	$sql = "	SELECT until
					FROM ".static::table."
					WHERE affected_user=?
						AND atype_id=?
						AND until > NOW()
						AND cancelled = 0
					ORDER BY until DESC";
			return $this->conn->run($sql,[$data['affected_user'],$data['atype_id']])->fetch(PDO::FETCH_COLUMN);
		}
		protected function cancel($data)
		{	$sql = " UPDATE ".static::table."
					SET cancelled = 1
					WHERE	atype_id=?
						AND affected_user=?
						AND chat_id=?";
			$this->conn->run($sql,[$data['atype_id'],$data['affected_user'],$data['chat_id']]);
		}
	}

	class Atype extends Storage
	{	const table = 'atypes';
		const fields = ['name'];
		const ez = ['name'];
	}

	class Chat extends Storage
	{	const table = 'chats';
		const fields = ['name','ctype_id','t_id','is_ok'];
		const store = ['name' => 'title', 't_id' => 'id'];
		const ez = ['t_id'];
		public function run($data = [])
		{	if(@$data['type'])
			{	$data['ctype_id'] = (new Ctype($this->conn,'ezid'))->run(['field' => 'name','value' => $data['type']]);
			}	elseif(@$data['obj']['type'])
			{	$data['ctype_id'] = (new Ctype($this->conn,'ezid'))->run(['field' => 'name','value' => $data['obj']['type']]);
			}
			return parent::run($data);
		}
	}

	class Ctype extends Storage
	{	const table = 'ctypes';
		const fields = ['name'];
		const ez = ['name'];
	}
	
	class Config extends Storage
	{	const table = 'configs';
		const fields = ['name','enabled','description','use_description','license_id'];
		const ez = ['name'];

		protected function all($data)
		{	$sql = " SELECT *
					FROM ".static::table."
					WHERE license_id=?";
			return $this->conn->run($sql,[$data['license_id']])->fetchAll();
		}
		
		protected function ezid($data)
		{	if(!defined('static::ez')) throw new Exception('Class not set up for ezid.');
			if($data['field'])
			{	if(!in_array($data['field'],static::ez)) throw new Exception('Column '.$data['field'].' not ez.');
				$field = $data['field'];
			}	else
			{	$field = static::ez[0];
			} 
			$sql = " SELECT id
					FROM ".static::table."
					WHERE $field=?
						AND license_id=?";
			return $this->conn->run($sql,[$data['value'],$data['license_id']])->fetch(PDO::FETCH_COLUMN);
		}
	}

	class Extra extends Storage
	{	const table = 'extras';
		const fields = ['code','media_id','deleted','caption','license_id'];

		protected function create($data)
		{	$data['code'] = strtolower(@$data['code']);
			return parent::create($data); 
		}
		protected function match($data)
		{	$sql = " SELECT m.mtype_id, m.reference, e.caption
					FROM extras as e
					LEFT JOIN media as m
						ON e.media_id = m.id
					WHERE e.code IN ('".implode("','",$data['obj'])."')
						AND e.deleted = 0
						".(@$data['license_id'] ? 'AND e.license_id=?' : 'AND e.license_id IS NULL')."
					LIMIT 1 ";
			return $this->conn->run($sql,(@$data['license_id'] ? [$data['license_id']] : NULL))->fetch();
		}
		protected function del($data)
		{	$sql = " UPDATE ".static::table."
					SET deleted=1
					WHERE code=? 
					".(@$data['license_id'] ? 'AND license_id=?' : 'AND license_id IS NULL');
					$params = [$data['code']];
					if(@$data['license_id']) $params[] = $data['license_id'];
			return $this->conn->run($sql,$params);
		}
		protected function _list($data)
		{	$sql = " SELECT DISTINCT code
					FROM extras
					WHERE deleted = 0
					ORDER BY code ASC ";
			return $this->conn->run($sql)->fetchAll(PDO::FETCH_COLUMN);
		}
	}
	
	class League extends Storage
	{	const table = "league";
		const fields = ['user_id','value','acting_id','license_id'];
		
		protected function sum($data)
		{	$sql = "SELECT u.first_name,u.last_name,u.t_id,SUM(l.value) as sum
					FROM ".static::table." as l
					INNER JOIN users as u
					ON l.user_id=u.id
					WHERE l.license_id=?
					GROUP BY l.user_id
					ORDER BY sum DESC, MAX(l.timestamp) DESC";
			return $this->conn->run($sql,[$data['license_id']])->fetchAll();
		}
		
		protected function topsnbottoms($data)
		{	$sql = "SELECT u.first_name,u.last_name,u.t_id,SUM(l.value) as sum
					FROM ".static::table." as l
					INNER JOIN users as u
					ON l.user_id=u.id
					WHERE l.license_id=?
					GROUP BY l.user_id
					ORDER BY sum DESC, MAX(l.timestamp) DESC LIMIT 10";
			$return[] = $this->conn->run($sql,[$data['license_id']])->fetchAll();

			$sql = "SELECT * FROM ( 
						SELECT u.first_name,u.last_name,u.t_id,SUM(l.value) as sum
						FROM ".static::table." as l
						INNER JOIN users as u
						ON l.user_id=u.id
						WHERE l.license_id=?
						GROUP BY l.user_id
						ORDER BY sum ASC LIMIT 10) sub
					ORDER BY sum DESC";
			$return[] = $this->conn->run($sql,[$data['license_id']])->fetchAll();
			return $return;
		}
	}
	
	class License extends Storage
	{	const table = "licenses";
		const fields = ['user_id','chat_id','sleep','disabled'];

		

		protected function create($data)
		{	$new = parent::create($data);
			$sql = " SELECT name, use_description
					FROM configs GROUP BY name ";
			$configs = $this->conn->run($sql)->fetchAll();
			foreach ($configs as $row)
			{	(new Config($this->conn,'create'))->run([
					"name"=>$row['name'],
					'use_description' => $row['use_description'],
					'license_id' => $new['id']
				]);
			}
			return $new;
		}

		protected function by_user($data)
		{	$sql = " SELECT *
					FROM ".static::table."
					WHERE user_id=? AND disabled = 0";
			return $this->conn->run($sql,[$data['user_id']])->fetchAll();
		}
		protected function by_chat($data)
		{	$sql = " SELECT l.*,c.t_id
					FROM ".static::table." as l
					INNER JOIN chats as c
						ON c.id = l.chat_id
					WHERE l.chat_id=?";
			return $this->conn->run($sql,[$data['chat_id']])->fetch();
		}
	}
	
	class LicenseUser extends Storage
	{	const table = "licenses_users";
		const fields = ['user_id','license_id','og','fem','colored','admin_id','protection_id','can_ui','unrepentant'];
		
		protected function can_ui($data)
		{	$sql =  " SELECT lu.license_id,lu.license_id,c.name
					FROM licenses_users as lu
					INNER JOIN licenses as l
						ON l.id = lu.license_id
					INNER JOIN chats as c
						ON l.chat_id = c.id
					WHERE lu.user_id = ?";
			return $this->conn->run($sql,[$data['user_id']])->fetchAll(PDO::FETCH_UNIQUE);
		}
	}

	class Media extends Storage
	{	const table = 'media';
		const fields = ['mtype_id','t_id','reference'];
		const store = ['t_id' => 'file_unique_id','reference' => 'file_id'];
		const ez = ['t_id'];
		public function run($data = [])
		{	if(@$data['type'])
			{	$data['mtype_id'] = (new Mtype($this->conn,'ezid'))->run(['field' => 'name','value' => $data['type']]);
			}
			return parent::run($data);
		}
	}

	class Message extends Storage
	{	const table = 'messages';
		const fields = ['user_id','text','t_id','media_id','timestamp','update_id','chat_id','reply_to_id','caption'];
		const store = ['t_id' => 'message_id'];
		const ez = ['t_id'];
		public function find($data)
		{	$sql = " SELECT *
					FROM ".static::table."
					WHERE chat_id = ?
						AND t_id = ? ";
			return $this->conn->run($sql,[$data['chat_id'],$data['message_id']])->fetch();
		}
	}

	class Mtype extends Storage
	{	const table = 'mtypes';
		const fields = ['name'];
		const ez = ['name'];
	}
	
	class Spam extends Storage
	{	const table = "spam";
		const fields = ["link"];
		
		protected function match($data)
		{	if(!is_array(@$data['links'])) return FALSE;
			$count_to_match = count($data['links']);
			$string = str_repeat(',?',$count_to_match-1);
			$sql = " SELECT COUNT(*)
					FROM ".static::table."
					WHERE link IN (?$string)";
			return $this->conn->run($sql,$data['links'])->fetch(PDO::FETCH_COLUMN);
		}
	}

	class Update extends Storage
	{	const table = 'updates';
		const fields = ['t_id','utype_id','json'];
		const store = ['t_id' => 'update_id'];
		const ez = ['t_id'];
		public function run($data = [])
		{	if(@$data['type'])
			{	$data['utype_id'] = (new Utype($this->conn,'ezid'))->run(['field' => 'name','value' => $data['type']]);
			}
			return parent::run($data);
		}
		protected function store($data)
		{	$update = parent::store($data);
			if(!@$data['obj']['message']) return;
			$chat = (new Chat($this->conn,'store'))->run([
				'obj' => $data['obj']['message']['chat']			
			]);
			$user = (new User($this->conn,'store'))->run([
				'obj' => $data['obj']['message']['from'],
				'license_id' => $data['license_id']			
			]);
			if(@$data['obj']['message']['audio'])
			{	$media_obj = $data['obj']['message']['audio'];
				$media_type = 'audio';
			}	elseif(@$data['obj']['message']['animation'])
			{	$media_obj = $data['obj']['message']['animation'];
				$media_type = 'animation';
			}	elseif(@$data['obj']['message']['sticker'])
			{	$media_obj = $data['obj']['message']['sticker'];
				$media_type = 'sticker';
			}	elseif(@$data['obj']['message']['photo'])
			{	$media_obj = array_pop($data['obj']['message']['photo']);
				$media_type = 'photo';
			}	elseif(@$data['obj']['message']['video'])
			{	$media_obj = $data['obj']['message']['video'];
				$media_type = 'video';
			}	elseif(@$data['obj']['message']['video_note'])
			{	$media_obj = $data['obj']['message']['video_note'];
				$media_type = 'video_note';
			}	elseif(@$data['obj']['message']['voice'])
			{	$media_obj = $data['obj']['message']['voice'];
				$media_type = 'voice';
			}	elseif(@$data['obj']['message']['document'])
			{	$media_obj = $data['obj']['message']['document'];
				$media_type = 'document';
			}	
			if(@$media_obj)
			{	$media = (new Media($this->conn,'store'))->run([
					'obj' => $media_obj,
					'type' => $media_type
				]);
			}
			$message = (new Message($this->conn,'store'))->run([
				'obj' => $data['obj']['message'],
				'user_id' => $user['id'],
				'chat_id' => $chat['id'],
				'update_id' => $update['id'],
				'media_id' => @$media['id'],
				't_id' => $chat['id'].'_'.$data['obj']['message']['message_id'],
				'reply_to_id' => @$data['obj']['message']['reply_to_message'] ? ((new Message($this->conn,'ezid'))->run([
					'field' => 't_id',
					'value' => $chat['id'].'_'.$data['obj']['message']['reply_to_message']['message_id']
				])) : NULL			
			]);
		}
	}

	class User extends Storage
	{	const table = 'users';
		const fields = ['t_id','username','admin_id','first_name','last_name','dm_id','og','fem','can_ui','first_ft','last_ft','milo'];
		const store = ['t_id' => 'id'];
		const ez = ['t_id','username'];
		const search = ['username','first_ft','last_ft'];

		protected function store($data)
		{	parent::store($data);
			$sql = "SELECT COUNT(*) FROM licenses_users where user_id=? and license_id=?";
			if(!$this->conn->run($sql,[$this->id,$data['license_id']])->fetch(PDO::FETCH_COLUMN))
			{	(new LicenseUser($this->conn,'create'))->run(['user_id' => $this->id, 'license_id' => $data['license_id']]);
			}
			return $this->read(['license_id' => $data['license_id']]); 
		}

		protected function read($data = [])
		{	if(!@$data['license_id'])
			{	$sql = "SELECT *
						FROM users
						WHERE id=?";
				return $this->conn->run($sql,[$this->id])->fetch();					
			}	else
			{	$sql = " SELECT u.id,u.first_name,u.last_name,u.t_id,u.username,u.milo,lu.id as lu_id,lu.admin_id,lu.og,
							lu.fem,lu.protection_id,lu.colored,lu.can_ui,lu.unrepentant
						FROM users as u
						INNER JOIN licenses_users as lu
							ON u.id=lu.user_id
						WHERE lu.license_id=? AND lu.user_id=?";
				return $this->conn->run($sql,[@$data['license_id'],$this->id])->fetch();					
			}
		}

		protected function all($data)
		{	$sql = "SELECT *
					FROM ".static::table."
					ORDER BY admin_id DESC,
						first_name ASC";
			return $this->conn->run($sql)->fetchAll();	
		}

		protected function dynamic($data)
		{	$skip = $data['offset'] ?: 0;
			$sql = "SELECT u.id,u.first_name,u.last_name,u.t_id,u.username,lu.id as lu_id,lu.admin_id,lu.og,
							lu.fem,lu.protection_id,lu.colored,lu.can_ui
						FROM users as u
						INNER JOIN licenses_users as lu
							ON u.id=lu.user_id
						WHERE lu.license_id=?
						ORDER BY admin_id DESC, first_name ASC
						LIMIT $skip, 200";
			return $this->conn->run($sql,[$data['license_id']])->fetchAll();	
		}

		protected function admin($data)
		{	$sql = " SELECT u.id,u.first_name,u.last_name,u.t_id,u.username,lu.id as lu_id,lu.admin_id,lu.og,
							lu.fem,lu.protection_id,lu.colored,lu.can_ui
						FROM users as u
						INNER JOIN licenses_users as lu
							ON u.id=lu.user_id
						WHERE lu.license_id=? AND lu.admin_id IS NOT NULL
						ORDER BY admin_id DESC, first_name ASC";
			return $this->conn->run($sql,[$data['license_id']])->fetchAll();	
		}
		protected function fem($data)
		{	$sql = "SELECT u.id,u.first_name,u.last_name,u.t_id,u.username,lu.id as lu_id,lu.admin_id,lu.og,
							lu.fem,lu.protection_id,lu.colored,lu.can_ui
						FROM users as u
						INNER JOIN licenses_users as lu
							ON u.id=lu.user_id
						WHERE lu.license_id=? AND lu.fem = 1
					ORDER BY	first_name ASC";
			return $this->conn->run($sql,[$data['license_id']])->fetchAll();	
		}
		protected function og($data)
		{	$sql = "SELECT u.id,u.first_name,u.last_name,u.t_id,u.username,lu.id as lu_id,lu.admin_id,lu.og,
							lu.fem,lu.protection_id,lu.colored,lu.can_ui
						FROM users as u
						INNER JOIN licenses_users as lu
							ON u.id=lu.user_id
						WHERE lu.license_id=? AND lu.og = 1
					ORDER BY first_name ASC";
			return $this->conn->run($sql,[$data['license_id']])->fetchAll();	
		}
		protected function history($data)
		{	$limit = $data['limit'] ? 'LIMIT 10' : '';
			$sql = " SELECT a.*, u.first_name as acting_first, u.last_name as acting_last, at.name as type, m.t_id as message, c.t_id as chat
					FROM actions as a
					LEFT JOIN users as u
						ON a.acting_user=u.id
					LEFT JOIN atypes as at
						ON a.atype_id=at.id
					LEFT JOIN chats as c
						ON a.chat_id = c.id
					LEFT JOIN messages as m
						ON a.message_id = m.id
					WHERE a.affected_user = ?
					ORDER BY timestamp DESC
					$limit ";
			return $this->conn->run($sql,[$this->id])->fetchAll();
		}
		protected function summary()
		{	$sql = " SELECT at.name, COUNT(*)
					FROM actions as a
					LEFT JOIN atypes as at
						ON a.atype_id=at.id
					WHERE affected_user=?
					GROUP BY at.name ";
			return $this->conn->run($sql,[$this->id])->fetchAll(PDO::FETCH_KEY_PAIR);
		}
	}

	class Utype extends Storage
	{	const table = 'utypes';
		const fields = ['name','from_web'];
		const ez = ['name'];
	}
	
	class Quip extends Storage
	{	const table = 'quips';
		const fields = ['qtype_id','text','active','license_id'];
		public function run($data = [])
		{	if(@$data['type'])
			{	$data['qtype_id'] = (new Qtype($this->conn,'ezid'))->run(['field' => 'name','value' => $data['type']]);
			}
			return parent::run($data);
		}
		public function active($data)
		{	$sql = "SELECT text
					FROM ".static::table."
					WHERE qtype_id=?
						AND license_id=?
						AND active=1 ";
			return $this->conn->run($sql,[$data['qtype_id'],$data['license_id']])->fetch(PDO::FETCH_COLUMN);
		}
		public function all_active($data)
		{	$sql = "SELECT text
					FROM ".static::table."
					WHERE qtype_id=?
						AND license_id=?
						AND active=1
					ORDER BY text ASC ";
			return $this->conn->run($sql,[$data['qtype_id'],$data['license_id']])->fetchAll(PDO::FETCH_COLUMN);
		}
		
		public function deactivate($data)
		{	$sql = "UPDATE ".static::table."
					SET active=0
					WHERE qtype_id=?
						AND license_id=?";
			$this->conn->run($sql,[$data['qtype_id'],$data['license_id']]);
		}
	}
	
	class Qtype extends Storage
	{	const table = 'qtypes';
		const fields = ['name'];
		const ez = ['name'];
	}
?>
