<?php
	/*
		ReqOut class is for sending cURL requests to Telegram Bot API, to trigger Bot actions like messages, updates, or
		inline keyboard updates.
		The class caches the outgoing request and the response from Telegram Bot API. These objects may be viewed by
		navigating a web browser to the web root.
				
		Invocation Parameters:
			method: the name of a method in the Telegram Bot API
		
		Run Parameters:
			body: either an array or a json_encoded string (inline keyboards with emoji requre special encoding, do it
																			before you pass it in)
		
		Return:
		
	*/

	class ReqOut
	{	public $response;
		
		public function __construct($method = NULL)
		{	$url = 'https://api.telegram.org/bot'.auth.'/';
			$this->ch = curl_init($url.$method);
			curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, TRUE);
		}
		
		public function run($body = NULL)
		{	if($body)
			{	if (is_array($body)) $body = json_encode($body);
				curl_setopt($this->ch, CURLOPT_POST, TRUE);
				curl_setopt($this->ch, CURLINFO_HEADER_OUT, true);
				curl_setopt($this->ch, CURLOPT_POSTFIELDS, $body);
				curl_setopt($this->ch, CURLOPT_HTTPHEADER,
					array(	'Content-Type: application/json',
				   	 		'Content-Length: '.strlen($body)) );
			}
			$this->response = curl_exec($this->ch);
			$cache = fopen($_SERVER['DOCUMENT_ROOT'].'/lastout.cache','wb');
			fwrite($cache, $body);
			fclose($cache);
			$cache = fopen($_SERVER['DOCUMENT_ROOT'].'/lastresponse.cache','wb');
			fwrite($cache, $this->response);
			fclose($cache);
			return json_decode($this->response,TRUE);
		}	
	}
?>