<?php
	/*
		HookIn class is for processing an incoming Telegram update (webhook), and preparing output(s) to the Telegram
		Bot API when appropriate.
		This class makes use of Storage instances for SQL operations, and ReqOut instances for cURL requests.
		Instantiate the class with the open PDO connection, then call this->run to execute.
		The update object from Telegram is accessed at php://input
		
		Invocation Parameters:
			conn: MyPDO object, the open database connection
		
		Run Parameters:

		Return:
		
		Methods are grouped as follows:
			construction methods - called by __construct(),
			run methods - this->run(), & a method each for message, callbackQuery, and 'lurking',
			service methods - access control, convenience methods for bot command & callbackQuery processing,
			output methods - for preparing & sending all the different types of output to Telegram Bot API,
			bot methods - one for each Bot Command in Telegram (need not be documented in Telegram Client)
	*/

	class HookIn
	{	public function __construct(MyPDO $conn,$update=NULL)
		{	$this->conn = $conn;
			$this->update = json_decode($update,TRUE) ?: json_decode(file_get_contents('php://input'),TRUE);
			if(@$this->update['message'] && strpos($this->update['message']['text'],'/!') === 0)
			{	$this->parseSilentCommand();
			}
			if(@$this->update['message']['entities'])
			{	$this->parseEntities($this->update['message']['entities']);
			}	elseif(@$this->update["message"]["caption_entities"])
			{	$this->parseCaptionEntities($this->update['message']['caption_entities']);	
			}	elseif(@$this->update['callback_query'])
			{	$this->cQuery = json_decode($this->update['callback_query']['data'],TRUE);
			}
			$chat_obj = $this->update['callback_query']['message']['chat'] ?: $this->update['message']['chat'];
			$this->chat = (new Chat($this->conn,'store'))->run(['obj' => $chat_obj]);
			$this->license = (new License($this->conn,'by_chat'))->run(['chat_id' => $this->chat['id']]);
			$this->parseFromUser();
			$this->db_from_user = (new User($this->conn,'ezread'))->run(['value' => $this->t_from_user,'license_id'=>$this->license['id']]);
			$this->isMilo = (@$this->db_from_user['milo'] || @$this->update['message']['sender_chat']['id'] == -1001401460680)
				? 1 : 0;
			$cache = fopen($_SERVER['DOCUMENT_ROOT'].'/lastupdate.cache','wb');
			fwrite($cache, json_encode($this->update));
			fclose($cache);
		}

//construction methods

		protected function parseCallbackEntities()
		{	if($this->update['callback_query']['message']['reply_to_message']['entities'])
			{	$this->parseEntities($this->update['callback_query']['message']['reply_to_message']['entities']);
			}
		}
		
		protected function parseSilentCommand()
		{	$arr = explode(' ',$this->update['message']['text'],2);
			$this->command = strtolower(ltrim($arr[0],'/!'));
			if(strpos($arr[0],'/!!') === 0) $this->silent = 1;
			$this->command_trail = $arr[1];
			(new ReqOut('deleteMessage'))->run([
				'chat_id' => $this->update['message']['chat']['id'],
				'message_id' => $this->update['message']['message_id']
			]);
		}

		protected function parseEntities($obj)
		{	foreach($obj as $row)
			{	switch($row['type'])
				{	case 'bot_command':
						if(@$this->command || strpos($this->update['message']['text'],'/') !== 0) break;
						$this->command = strtolower($this->parseEntity($row,'\/'));
						$this->command = (strstr($this->command,'@',TRUE)) ?: $this->command;
						$this->command_trail = ltrim(explode($this->command,$this->update['message']['text'],2)[1])
							?: NULL; 
						break;
					case 'hashtag':
						$this->hashtags[] = strtolower($this->parseEntity($row,'#'));
						break;
					case 'mention':
						$this->mentions[] = $this->parseEntity($row,'@');
						break;
					case 'text_link':
						$this->urls[] = $row['url'];
					case 'url':
						$this->urls[] = $this->parseEntity($row,'');
						break;
					case 'text_mention':
						$this->text_mentions[] = [ 'id' => $row['user']['id'], 'name' => $row['user']['first_name']];
						break;
				}
			}
			if($this->command_trail && ($this->text_mentions || $this->mentions))
			{	$this->command_trail = ltrim(explode($this->mentions[0] ?: $this->text_mentions[0]['name'],$this->command_trail,2)[1]);
			}
		}

		protected function parseCaptionEntities($obj)
		{	foreach($obj as $row)
			{	switch($row['type'])
				{	case 'mention':
						$this->mentions[] = $this->parseCaptionEntity($row,'@');
						break;
					case 'url':
						$this->urls[] = $this->parseCaptionEntity($row,'');
						break;
					case 'text_url':
						$this->urls[] = $row['url'];
						$this->urls[] = $this->parseCaptionEntity($row,'');
						break;
					case 'text_mention':
						$this->text_mentions[] = [ 'id' => $row['user']['id'], 'name' => $row['user']['first_name']];
						break;
				}
			}
			if($this->command_trail && ($this->text_mentions || $this->mentions))
			{	$this->command_trail = ltrim(explode($this->mentions[0] ?: $this->text_mentions[0]['name'],$this->command_trail,2)[1]);
			}
		}

		protected function parseEntity($arr,$trim)
		{	$t = mb_convert_encoding($this->update['message']['text'],"UTF-16","UTF-8");
			return ltrim(mb_convert_encoding(substr($t,$arr['offset']*2,$arr['length']*2),"UTF-8","UTF-16"),$trim);
		}
		
		protected function parseCaptionEntity($arr,$trim)
		{	$t = mb_convert_encoding($this->update['message']['caption'],"UTF-16","UTF-8");
			return ltrim(mb_convert_encoding(substr($t,$arr['offset']*2,$arr['length']*2),"UTF-8","UTF-16"),$trim);
		}

		
		protected function parseFromUser()
		{	$this->t_from_user = ($this->update['callback_query'])
				? $this->update['callback_query']['from']['id']
				: $this->update['message']['from']['id'];
		}
		
// run methods
	
		public function run()
		{	if((!$this->chat_ok() && $this->command != 'begin') || ($this->license && $this->license['sleep'] && $this->command != 'wakeup'))
			{	return;
			}	else 
			{	if($this->command)
				{	$this->doCommand();
				}	elseif($this->update['callback_query'])
				{	$this->doCallback();
				}	else
				{	$this->lurk();
				}
				$this->dbStore();
			}
		}
		
		protected function doCommand()
		{	if(method_exists($this,$this->command) && $this->auth()) 
			{	$this->{$this->command}();
			} 	else 
			{	if(!$this->db_from_user['admin_id']) $this->_reprimand();
			}
		}
		
		protected function doCallback()
		{	if($this->cQuery == "sorry") {
				$affected = $this->db_from_user;
				$this->silent = 1;
				$this->gag(['affected' => $affected,'duration'=>'24 hours']);
//				$this->answerQuery(['text' => 'Muted 24 Hours.']);
			} elseif(method_exists($this,$this->cQuery['method']) && $this->auth()) 
			$this->{$this->cQuery['method']}();
			else
			$this->answerQuery(['text' => 'No.']); 
		}
		
		protected function lurk()
		{	$this->_spam();
			if(@$this->hashtags) $this->matchExtra();
			if(@$this->update['message']['new_chat_member']) $this->_welcome();
			if(@$this->update['message']) $this->_pleb();
			if(@$this->update['message']) $this->_repent();
			if(@$this->update['message']) $this->_decolonize();
			if(@$this->update['message']) $this->_alalah();
			if(@$this->update['message']) $this->_badword();
			if(@$this->update['message']) $this->_trigger();
			if(@$this->update['message'] && @$this->isMilo) $this->_apologize();
			if(@$this->update['message']['sender_chat'] && !@$this->isMilo) {
				(new ReqOut('deleteMessage'))->run([
					'chat_id' => $this->update['message']['chat']['id'],
					'message_id' => $this->update['message']['message_id']
				]);
			}
		}
		
// service methods 

		protected function auth()
		{	$commands['papa'] = ['newkey','nokey'];
			$commands['licensee'] = ['sleep','wakeup','badword'];
			$commands[4] = [

				'welcome','ban','demote','deport','unban','kick','fem','og',
				'noegirls','egirls','ui','cry','dog','nodog','delete',
				'excommunicate','saywhat','reprimand','chattel','ax',
				'ask','quiettime','diverse','mlk','spam','repent','unrepent','badwords','trigger'

			];
			$commands[3] = [

				'extra','promote','gag','mute','history','reverse','nowarns','ungag','unmute'

			];
			$commands[2] = [
			
				'timeout','skinnyfat','warn','parler','merit','demerit','league','table'
			];
			
			$commands[1] = [];

			$allowed = ['help','start','begin'];

			if($this->db_from_user['id'] == $this->license['user_id'] || @$this->isMilo)
			{	if(@$this->isMilo) $allowed = array_merge($allowed,$commands['papa']);
				$allowed = array_merge($allowed,$commands['licensee']);
				$this->db_from_user['admin_id'] = 4;
			}
			switch(@$this->db_from_user['admin_id'])           
			{	case 4:
					$allowed = array_merge($allowed,$commands[4]);
				case 3:
					$allowed = array_merge($allowed,$commands[3]);
				case 2:
					$allowed = array_merge($allowed,$commands[2]);
					break;
			}
			if(!in_array(@$this->command,$allowed) && !in_array(@$this->cQuery['method'],$allowed))
			{	return FALSE;
			}
			return TRUE;
		}

		protected function chat_ok()
		{	$chat_obj = $this->update['callback_query']['message']['chat'] ?: $this->update['message']['chat'];
			if($chat_obj['type'] == 'private') return 1;
			return ($this->license && !@$this->license['disabled']) ? 1 : 0;
		}

		protected function _welcome()
		{	$config = (new Config($this->conn,'ezread'))->run(['value' => 'welcome','license_id'=>$this->license['id']]);			
			if($config['enabled'] && $text = (new Quip($this->conn,'active'))->run([
				'type' => 'welcome',
				'license_id'=>$this->license['id']
			]))
			{	$format = '[%s](tg://user?id=%d)';
				$replace = $this->formatMention(
					$this->update['message']['new_chat_member']['first_name'],
					$this->update['message']['new_chat_member']['id']
				); 
				$output = str_replace(':name',$replace,$text);
				$replace = $this->update['message']['chat']['title'];
				$output = str_replace(':chat',$replace,$output);
				$this->markMsg(['text' => $output]);
			}			
		}
		
		protected function _badword()
		{	$badwords = (new Quip($this->conn,'all_active'))->run([
				'license_id' => $this->license['id'],
				'type' => 'badword'
			]);
			if(!$badwords) return;
			if(array_intersect($badwords,explode(' ',strtolower($this->update['message']['text']))))
			{	//$this->quickRepMsg(['text' => 'Thats a baaad word.' ]);
				$user = (new User($this->conn,'store'))->run(['obj' => $this->update['message']['from'],'license_id' => $this->license['id']]);
				$this->warn(['affected' => $user, 'note' => 'language']);
			}			
		}

		protected function _trigger()
		{	$match = (new Extra($this->conn,'match'))->run([
				'obj' => explode(' ',strtolower($this->update['message']['text'])),
				'license_id'=>$this->license['id']
			]);
			if($match)
			{	$match['no_follow_reply'] = 1;
				$this->sendExtra($match);
			}
		}
		
		protected function _apologize()
		{	if(strpos($this->update['message']['text'],'Apologize') !== 0) return;
			$keyboard = [[['text' => "I'M SORRY", 'callback_data' => json_encode('sorry')],['text'=>'BAN ME','callback_data'=>json_encode('sorry')]]];	
			$this->markMsg([
				'text'=>'Apologize.',
				'inline_keyboard'=>$keyboard,
				'reply_to_message'=>$this->update['message']['reply_to_message']['message_id']
			]); 			
		}

		protected function _reprimand()
		{	if($text = (new Quip($this->conn,'all_active'))->run([
				'type' => 'reprimand',
				'license_id'=>$this->license['id']
			]))
			{	$rand = array_rand($text);
				$this->quickRepMsg(['text' => $text[$rand]]);
			}
			$user = (new User($this->conn,'store'))->run(['obj' => $this->update['message']['from'],'license_id' => $this->license['id']]);
			(new ReqOut('restrictChatMember'))->run([
				'chat_id' => $this->update['message']['chat']['id'],
				'user_id' => $this->update['message']['from']['id'],
				'permissions' => [
					'can_send_messages' => FALSE
				],
				'until_date' => (time()+3600)
			]);
			$chat = (new Chat($this->conn,'ezid'))->run(['value'=> $this->update['message']['chat']['id']]);	
			$message = (new Message($this->conn,'store'))->run([
				'obj' => $this->update['message'],
				'chat_id' => $chat,
				't_id' => $chat.'_'.$this->update['message']['message_id']
			]);
			$action = (new Action($this->conn,'create'))->run([
				'type' => 'timeout',
				'acting_user' => null,
				'affected_user' => $user['id'],
				'notes' => '',
				'until' => (time()+3600),
				'chat_id' => $chat,
				'message_id' => $message['id']			
			]);
			$this->silent = TRUE;
			$this->demerit(['affected' => $user]);			
		}
		
		protected function _pleb()
		{	$config = (new Config($this->conn,'ezread'))->run(['value' => 'og mode','license_id'=>$this->license['id']]);
			if(!$config['enabled']) return;
			$user = (new User($this->conn,'store'))->run(['obj' => $this->update['message']['from'],'license_id' => $this->license['id']]);
			if(!$user['og']  && !@$affected['protection_id'])
			{	(new ReqOut('deleteMessage'))->run([
					'chat_id' => $this->update['message']['chat']['id'],
					'message_id' => $this->update['message']['message_id']
				]);
				(new ReqOut('restrictChatMember'))->run([
					'chat_id' => $this->update['message']['chat']['id'],
					'user_id' => $this->update['message']['from']['id'],
					'permissions' => [
						'can_send_messages' => FALSE
					],
					'until_date' => (time()+3600)
				]);
				$chat = (new Chat($this->conn,'ezid'))->run(['value'=> $this->update['message']['chat']['id']]);	
				$message = (new Message($this->conn,'store'))->run([
					'obj' => $this->update['message'],
					'chat_id' => $chat,
					't_id' => $chat.'_'.$this->update['message']['message_id']
				]);
				$action = (new Action($this->conn,'create'))->run([
					'type' => 'timeout',
					'acting_user' => null,
					'affected_user' => $user['id'],
					'notes' => '',
					'until' => (time()+3600),
					'chat_id' => $chat,
					'message_id' => $message['id']			
				]);
			}
/*			if($user['og'] !== 0 && $user['og'] != 1 && $config['enabled'])
			{	(new ReqOut('restrictChatMember'))->run([
					'chat_id' => $this->update['message']['chat']['id'],
					'user_id' => $this->update['message']['from']['id'],
					'permissions' => $this->setPerms(0)
				]);
				(new User($this->conn,'edit',$user['id']))->run(['og' => 0]); 
			}
			if($user['og'] != 1 && $user['og'] !== NULL && !$config['enabled'])
			{	(new ReqOut('restrictChatMember'))->run([
					'chat_id' => $this->update['message']['chat']['id'],
					'user_id' => $this->update['message']['from']['id'],
					'permissions' => $this->setPerms(1)
				]);
				(new User($this->conn,'edit',$user['id']))->run(['og' => NULL]); 
			}
*/
		}

		protected function _decolonize()
		{	$config = (new Config($this->conn,'ezread'))->run(['value' => 'no coloreds','license_id'=>$this->license['id']]);
			$config2 = (new Config($this->conn,'ezread'))->run(['value' => 'no whites','license_id'=>$this->license['id']]);
			if(!$config['enabled'] && !$config2['enabled']) return;
			$user = (new User($this->conn,'store'))->run(['obj' => $this->update['message']['from'],'license_id' => $this->license['id']]);
			if((($config['enabled'] && $user['colored']) || ($config2['enabled'] && !$user['colored']))
				&& @$user['protection_id'] !== 1)
			{	(new ReqOut('deleteMessage'))->run([
					'chat_id' => $this->update['message']['chat']['id'],
					'message_id' => $this->update['message']['message_id']
				]);
				(new ReqOut('restrictChatMember'))->run([
					'chat_id' => $this->update['message']['chat']['id'],
					'user_id' => $this->update['message']['from']['id'],
					'permissions' => [
						'can_send_messages' => FALSE
					],
					'until_date' => (time()+3600)
				]);
				$chat = (new Chat($this->conn,'ezid'))->run(['value'=> $this->update['message']['chat']['id']]);	
				$message = (new Message($this->conn,'store'))->run([
					'obj' => $this->update['message'],
					'chat_id' => $chat,
					't_id' => $chat.'_'.$this->update['message']['message_id']
				]);
				$action = (new Action($this->conn,'create'))->run([
					'type' => 'timeout',
					'acting_user' => null,
					'affected_user' => $user['id'],
					'notes' => 'racial sensitivity',
					'until' => (time()+7200),
					'chat_id' => $chat,
					'message_id' => $message['id']			
				]);
			}
		}
		
		protected function _alalah()
		{	$user = (new User($this->conn,'store'))->run(['obj' => $this->update['message']['from'],'license_id' => $this->license['id']]);
			if(!$user['fem'] || $user['protection_id'] === 1) return;
			$sharia = (new Config($this->conn,'ezread'))->run(['value' => 'milo time utc offset','license_id'=>$this->license['id']]);
			$noegirls = (new Config($this->conn,'ezread'))->run(['value' => 'no e-girls','license_id'=>$this->license['id']]);
			if(!$sharia['enabled'] && !$noegirls['enabled']) return;
			if($sharia['enabled']  
				&& (new DateTime())->setTimeZone((new DateTimeZone($sharia['description'])))->format('l') == 'Tuesday') 
			{ 	$duration = "72 hours";
				$type = 'gag';
				$note = "#ShariaTuesday";
//				$output = $this->update['message']['from']['first_name'].' gagged. #ShariaTuesday';
			}	elseif($noegirls['enabled'])
			{	$duration = "1 hour";
				$type = 'timeout';
				$note = "#NoEgirls";
//				$output = 'Timeout for '.$this->update['message']['from']['first_name'].'. #NoEgirls';
			}	else
			{	return;
			}
 	
			$date = new DateTime();
			$date->setTimestamp(time()); 
			$interval = DateInterval::createFromDateString($duration);
			$date->add($interval);
			$until = $date->getTimestamp();
			(new ReqOut('restrictChatMember'))->run([
				'chat_id' => $this->update['message']['chat']['id'],
				'user_id' => $this->update['message']['from']['id'],
				'permissions' => [
					'can_send_messages' => FALSE
				],
				'until_date' => $until
			]);

//			$this->markMsg(['text' => $output]);
			$this->sendExtra([
				"mtype_id"=>3,
				"reference"=>'CgACAgQAAx0CUDw9dQABKowmYaWZzrkCAoat6CSvJtHuGTNsQAUAAm8CAAIbQYxSfLo4S90hA3AiBA',
				"no_follow_reply"=>1
			]);
			$chat = (new Chat($this->conn,'ezid'))->run(['value'=> $this->update['message']['chat']['id']]);	
			$message = (new Message($this->conn,'store'))->run([
				'obj' => $this->update['message'],
				'chat_id' => $chat,
				't_id' => $chat.'_'.$this->update['message']['message_id'],
			]);
			$action = (new Action($this->conn,'create'))->run([
				'type' => $type,
				'acting_user' => null,
				'affected_user' => $user['id'],
				'notes' => $note,
				'until' => date("Y-m-d H:i:s",$until),
				'chat_id' => $chat,
				'message_id' => $message['id']			
			]);
//			if(!$user['og']) (new User($this->conn,'edit',$user['id']))->run(['og' => NULL]);
		}

		protected function _repent()
		{	$user = (new User($this->conn,'store'))->run([
				'obj' => $this->update['message']['from'],
				'license_id' => $this->license['id']
			]);
			if(!$user['unrepentant'] || $user['protection_id'] === 1) return;
			$this->quickRepMsg(['text' => "Repent."]);
		}
		
		protected function _spam()
		{	if(!$this->mentions && !$this->urls) return FALSE;
			$arr = array_merge($this->mentions ?: [],$this->urls ?: []);
			if((new Spam($this->conn,'match'))->run(["links" => $arr]))
			{	(new ReqOut('deleteMessage'))->run([
					'chat_id' => $this->update['message']['chat']['id'],
					'message_id' => $this->update['message']['message_id']
				]);
				(new ReqOut('restrictChatMember'))->run([
					'chat_id' => $this->update['message']['chat']['id'],
					'user_id' => $this->update['message']['from']['id'],
					'permissions' => [
						'can_send_messages' => FALSE
					]
				]);
				$user = (new User($this->conn,'store'))->run([
					'obj' => $this->update['message']['from'],
					'license_id' => $this->license['id']
				]);
				$chat = (new Chat($this->conn,'ezid'))->run(['value'=> $this->update['message']['chat']['id']]);	
				$message = (new Message($this->conn,'store'))->run([
					'obj' => $this->update['message'],
					'chat_id' => $chat,
					't_id' => $chat.'_'.$this->update['message']['message_id'],
				]);
				$action = (new Action($this->conn,'create'))->run([
					'type' => 'gag',
					'acting_user' => null,
					'affected_user' => $user['id'],
					'notes' => 'spam',
					'chat_id' => $chat,
					'message_id' => $message['id']			
				]);
				foreach ($arr as $link)
				{	(new Spam($this->conn,'create'))->run(["link" => $link]);
				}
			}
		}

		protected function mediaVars($mtype_id)
		{	switch($mtype_id)
			{	case 1:
					$key = 'sticker';
					$name = 'Sticker';
					break;
				case 2:
					$key = 'photo';
					$name = 'Photo';
					break;
				case 3:
					$key = 'animation';
					$name = 'Animation';
					break;
				case 4:
					$key = 'audio';
					$name = 'Audio';
					break;
				case 5:
					$key = 'document';
					$name = 'Document';
					break;
				case 6:
					$key = 'video';
					$name = 'Video';
					break;
				case 7:
					$key = 'video_note';
					$name = 'VideoNote';
					break;
				case 8:
					$key = 'voice';
					$name = 'Voice';
					break;
			}
			return [$key,$name];		
		}
		
		protected function formatMention($name,$id)
		{	$format = '_[%s](tg://user?id=%d)_';
			return sprintf($format,$name,$id); 
		}
		
		protected function matchExtra()
		{	$match = (new Extra($this->conn,'match'))->run(['obj' => $this->hashtags]);
			if($match)
			{	$this->sendExtra($match);
			}
		}
		
		protected function dbStore()
		{	$update = (new Update($this->conn,'store'))->run([
				'obj' => $this->update,
				'type' => 'message',
				'json' => json_encode($this->update),
				'license_id' => $this->license['id']
			]);
		}
		
		protected function setPerms(/*$og*/)
		{	$perms = [
				'can_send_messages' => TRUE,
				'can_send_media_messages' => TRUE,
				'can_send_other_messages'	=> TRUE,
				'can_add_web_page_previews' => TRUE		
			];
			return $perms;
		}

// output methods

		protected function quickMsg($data)
		{	$object = [
				'chat_id' => $data['chat_id'] ?: $this->update['message']['chat']['id'],
				'text' => $data['text']
			];
			(new ReqOut('sendMessage'))->run($object);
		}

		protected function quickRepMsg($data)
		{	$object = [
				'chat_id' => $this->update['message']['chat']['id'],
				'text' => $data['text'],
				'reply_to_message_id' => $this->update['message']['message_id']
			];
			(new ReqOut('sendMessage'))->run($object);
		}

		protected function markMsg($data)
		{	$chat_id = $this->update['message']['chat']['id'] ?: $this->update['callback_query']['message']['chat']['id'];
			$chat_id = $data['chat_id'] ?: $chat_id;
			$object = [
				'chat_id' => $chat_id,
				'text' => (@$data['no_force_italic'] ? '' : '_' ).$data['text'].(@$data['no_force_italic'] ? '' : '_' ), 
				'parse_mode' => $data['mode'] ?: 'Markdown'
			];
			if(@$data['inline_keyboard']) $object['reply_markup'] = [ 'inline_keyboard' => $data['inline_keyboard']];
			if(@$data['reply_to_message']) $object['reply_to_message_id'] = $data['reply_to_message'];
			return (new ReqOut('sendMessage'))->run($object);
		}

		protected function errMsg()
		{	$object = [ 
				'chat_id' => $this->update['message']['chat']['id'],
				'text' => "Big __oof__.", 
				'parse_mode' => 'Markdown'
			];
			(new ReqOut('sendMessage'))->run($object);
		}

		protected function repMsgKeyboard($data)
		{	$object = [
				'chat_id' => $this->update['message']['chat']['id'],
				'text' => $data['text'],
				'reply_to_message_id' => $this->update->message->message_id,
				'reply_markup' => [
					'keyboard' => $data['keyboard'],
					'one_time_keyboard' => TRUE,
					'selective' => TRUE
				] 
			];
			(new ReqOut('sendMessage'))->run($object);
		}
		
		protected function sendExtra($data)
		{	$object = [ 
				'chat_id' => $this->update['message']['chat']['id'],
				'reply_to_message_id' => $this->update['message']['reply_to_message']['message_id'] 
					?: $this->update['message']['message_id'],
				'parse_mode' => 'Markdown'
			];
			if(@$data["no_follow_reply"]) $object['reply_to_message_id'] = $this->update['message']['message_id'];
			if(@$data['mtype_id'])
			{	list($key,$name) = $this->mediaVars($data['mtype_id']);
				$object[$key] = $data['reference'];
				if(@$data['caption']) $object['caption'] = $data['caption'];
				if(strpos($this->update['message']['text'],'#') === 0 && explode($this->hashtags[0],$this->update['message']['text'],2)[1])
					$object['caption'] = ltrim(explode($this->hashtags[0],$this->update['message']['text'],2)[1]);
				(new ReqOut("send$name"))->run($object);
			}	else
			{	$object['text'] = $data['caption'];
				(new ReqOut("sendMessage"))->run($object);
			}
		}

		protected function answerQuery($data)
		{	$object = [
				'callback_query_id' => $this->update['callback_query']['id'],
				'text' => $data['text']
			];
			(new ReqOut('answerCallbackQuery'))->run($object);
		}
		
		protected function intent($data)
		{	$object = [
				'chat_id' => $this->update['message']['chat']['id'],
				'action' => $data['action']
			];
			(new ReqOut('sendChatAction'))->run($object);
		}
		
		protected function updateMsg($data)
		{	$object = [
				'chat_id' => $data['chat_id'],
				'message_id' => $data['message_id']
			];
			if($data['text']) $object['text'] = $data['text'];
			if($data['inline_keyboard']) $object['reply_markup'] = ['inline_keyboard' => $data['inline_keyboard']];
			if($data['no_keyboard']) $object['reply_markup'] = [ 'inline_keyboard' => []];
			(new ReqOut('editMessageText'))->run($object);
		}
		
// bot methods
		
		protected function help()
		{	$this->quickMsg(['text' => 'Help yourself, boomer.']);
		}
		
		protected function start()
		{	$this->markMsg(['text' => '_Sup._']);
		}
		
		protected function sleep()
		{	(new License($this->conn,'edit',$this->license['id']))->run(['sleep' => 1]);
			if(!@$this->silent) $this->markMsg(['text' => 'Goodnight.']); 
		}
		
		protected function wakeup()
		{	(new License($this->conn,'edit',$this->license['id']))->run(['sleep' => 0]);
			if(!@$this->silent) $this->markMsg(['text' => "I'm awake."]);	
		}
		
		protected function begin()
		{	if($this->license)
			{	return;
			}	elseif(!(new License($this->conn,'by_user'))->run(['user_id' => $this->db_from_user['id']]))
			{	return;	
			}	else
			{	$this->license = (new License($this->conn,'create'))->run([
					'user_id' => $this->db_from_user['id'],
					'chat_id' => $this->chat['id']
				]);
				$output = "Hello.";
			}
			if(@$output && !@$this->silent) $this->markMsg(['text' => $output]); 
		}		
		
		protected function ui()
		{	$login_obj = ['url' => ui_domain ];
			$keyboard = [[['text' => 'Go', 'login_url' => $login_obj]]];
			if($this->update['message']['chat']['type'] !== 'private') return; 	
			$this->markMsg(['text' => 'webUI', 'inline_keyboard' => $keyboard]);
		}
		
		protected function noegirls()
		{	$config = (new Config($this->conn,'ezread'))->run(['value' => 'no e-girls','license_id'=>$this->license['id']]);
			if($config['enabled'])
			{	$output = 'E-girls are currently not allowed.';
			}	else
			{	(new Config($this->conn,'edit',$config['id']))->run(['enabled' => 1]);
				$output = 'Warning: No e-girls.';			
			}
			if($output && !$this->silent) $this->markMsg(['text' => $output]);
		}

		protected function egirls()
		{	$config = (new Config($this->conn,'ezread'))->run(['value' => 'no e-girls','license_id'=>$this->license['id']]);
			if(!$config['enabled'])
			{	$output = 'E-girls are currently allowed.';
			}	else
			{	(new Config($this->conn,'edit',$config['id']))->run(['enabled' => 0]);
				$output = 'E-girls allowed.';			
			}
			if($output && !$this->silent) $this->markMsg(['text' => $output]);
		}
		
		protected function og()
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
			) $output = "";
			else
			{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
				{	$affected = (new User($this->conn,'ezread'))->run([
						'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
						'license_id' => $this->license['id']
					]);
				}	else
				{	$affected = (new User($this->conn,'ezread'))->run([
						'field' => 'username',
						'value' => $this->mentions[0],
						'license_id' => $this->license['id']
					]);
				}
				if(!$affected) return;
				$mention = $this->formatMention($affected['first_name'],$affected['t_id']);
				if(@$affected['og'])
				{	if(strpos($this->command_trail,'del') === FALSE)
					{	$output = $affected['first_name']." is already og. '/og del' to remove.";
					}	else
					{	(new LicenseUser($this->conn,'edit',$affected['lu_id']))->run(['og' => 0]);
						(new ReqOut('restrictChatMember'))->run([
							'chat_id' => $this->update['message']['chat']['id'],
							'user_id' => $affected['t_id'],
							'permissions' => $this->setPerms(/*0*/)
						]);
						$output = "$mention is no longer og.";
					}
				}	else
				{	if(strpos($this->command_trail,'del') !== FALSE)
					{	$output = "$mention is not og.";
					}	else 
					{	(new LicenseUser($this->conn,'edit',$affected['lu_id']))->run(['og' => 1]);
						(new ReqOut('restrictChatMember'))->run([
							'chat_id' => $this->update['message']['chat']['id'],
							'user_id' => $affected['t_id'],
							'permissions' => $this->setPerms(/*1*/)
						]);
						$output = "$mention is an OG. Defy at your own risk.";
					}
				}
			}
			if(@$output && !@$this->silent) $this->markMsg(['text' => $output]);
		}
		
		protected function dog()
		{	$config = (new Config($this->conn,'ezread'))->run(['value' => 'og mode','license_id'=>$this->license['id']]);
			if(!$config['enabled'])
			{	$output = "Common folk: Fall silent. This is a time for heroes.";
				$this->markMsg(['text' => $output]);
				(new Config($this->conn,'edit',$config['id']))->run(['enabled' => 1]);
			}
		}
		
		protected function nodog()
		{	$config = (new Config($this->conn,'ezread'))->run(['value' => 'og mode','license_id'=>$this->license['id']]);
			if($config['enabled'])
			{	$output = "Weep! Weep and despair! The rabid, unwashed hordes return.";
				$this->markMsg(['text' => $output]);
				(new Config($this->conn,'edit',$config['id']))->run(['enabled' => 0]);
			}
		}

		protected function quiettime()
		{	$config = (new Config($this->conn,'ezread'))->run(['value' => 'no coloreds','license_id'=>$this->license['id']]);
			$config2 = (new Config($this->conn,'ezread'))->run(['value' => 'no whites','license_id'=>$this->license['id']]);
			(new Config($this->conn,'edit',$config['id']))->run(['enabled' => 1]);
			(new Config($this->conn,'edit',$config2['id']))->run(['enabled' => 0]);
			$output = "ENTERING WHITE MODE. PLEASE HAVE YOUR BONE CHINA AND EVERYTHING BAGELS READY AND STOP YELLING AT THE TELEVISION.";
			if(!@$this->silent) $this->markMsg(['text' => $output]);
		}

		protected function diverse()
		{	$config = (new Config($this->conn,'ezread'))->run(['value' => 'no coloreds','license_id'=>$this->license['id']]);
			$config2 = (new Config($this->conn,'ezread'))->run(['value' => 'no whites','license_id'=>$this->license['id']]);
			(new Config($this->conn,'edit',$config['id']))->run(['enabled' => 0]);
			(new Config($this->conn,'edit',$config2['id']))->run(['enabled' => 1]);
			$output = "TO CELEBRATE DIVERSITY, WHITE PEOPLE ARE BANNED FROM POSTING UNTIL FURTHER NOTICE.";
			if(!@$this->silent) $this->markMsg(['text' => $output]);
		}

		protected function mlk()
		{	$config = (new Config($this->conn,'ezread'))->run(['value' => 'no coloreds','license_id'=>$this->license['id']]);
			$config2 = (new Config($this->conn,'ezread'))->run(['value' => 'no whites','license_id'=>$this->license['id']]);
			(new Config($this->conn,'edit',$config['id']))->run(['enabled' => 0]);
			(new Config($this->conn,'edit',$config2['id']))->run(['enabled' => 0]);
			$output = "INSPIRED BY THE FOUL RACIST IDEOLOGY OF MARTIN LUTHER KING, ALL MAY NOW SPEAK AND BE JUDGED BY THEIR WORDS INSTEAD OF THEIR MELANIN.";
			if(!@$this->silent) $this->markMsg(['text' => $output]);
		}

		protected function fem()
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
			) $output = "";
			else
			{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
				{	$affected = (new User($this->conn,'ezread'))->run([
						'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
						'license_id' => $this->license['id']
					]);
				}	else
				{	$affected = (new User($this->conn,'ezread'))->run([
						'field' => 'username',
						'value' => $this->mentions[0],
						'license_id' => $this->license['id']
					]);
				}
				if(!$affected) return;
				if($affected['fem'])
				{	$output = $affected['first_name']." is already branded femoid.";
				}	else
				{	(new LicenseUser($this->conn,'edit',$affected['lu_id']))->run(['fem' => 1]);
					$mention = $this->formatMention($affected['first_name'],$affected['t_id']);
					$output = "$mention hereby branded FEMOID.";
				}
			}
			if(@$output && !@$this->silent) $this->markMsg(['text' => $output]);
		}

		protected function ax()
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
			) $output = "";
			else
			{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
				{	$affected = (new User($this->conn,'ezread'))->run([
						'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
						'license_id' => $this->license['id']
					]);
				}	else
				{	$affected = (new User($this->conn,'ezread'))->run([
						'field' => 'username',
						'value' => $this->mentions[0],
						'license_id' => $this->license['id']
					]);
				}
				if(!$affected) return;
				if($affected['colored'])
				{	$output = "Yes, ".$affected['first_name']." is colored.";
				}	else
				{	(new LicenseUser($this->conn,'edit',$affected['lu_id']))->run(['colored' => 1]);
					$mention = $this->formatMention($affected['first_name'],$affected['t_id']);
					$output = "$mention runs faster than you.";
				}
			}
			if(@$output && !@$this->silent) $this->markMsg(['text' => $output]);
		}

		protected function ask()
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
			) $output = "";
			else
			{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
				{	$affected = (new User($this->conn,'ezread'))->run([
						'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
						'license_id' => $this->license['id']
					]);
				}	else
				{	$affected = (new User($this->conn,'ezread'))->run([
						'field' => 'username',
						'value' => $this->mentions[0],
						'license_id' => $this->license['id']
					]);
				}
				if(!$affected) return;
				if(!$affected['colored'])
				{	$output = $affected['first_name']." is already white.";
				}	else
				{	(new LicenseUser($this->conn,'edit',$affected['lu_id']))->run(['colored' => 0]);
					$mention = $this->formatMention($affected['first_name'],$affected['t_id']);
					$output = "$mention is now white, and can get a loan.";
				}
			}
			if(@$output && !@$this->silent) $this->markMsg(['text' => $output]);
		}
		
		protected function promote()
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
			) $output = "";
			else 
			{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
				{	$affected = (new User($this->conn,'ezread'))->run([
						'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
						'license_id' => $this->license['id']
					]);
				}	else
				{	$affected = (new User($this->conn,'ezread'))->run([
						'field' => 'username',
						'value' => $this->mentions[0],
						'license_id' => $this->license['id']
					]);
				}
				if(!$affected || !$this->command_trail) return;
				$admin_level = (new Admin($this->conn,'ezread'))->run(['value' => $this->command_trail]);
				if(!$admin_level || $this->db_from_user['admin_id'] < $admin_level['id']) return;
				(new LicenseUser($this->conn,'edit',$affected['lu_id']))->run(['admin_id' => $admin_level['id']]);
				$output = "I've promoted ".$this->formatMention($affected['first_name'],$affected['t_id'])." to ".$admin_level['name'].' admin.';
			}
			if($output && !@$this->silent) $this->markMsg(['text' => $output]);
		}

		protected function demote()
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
			) $output = "";
			else 
			{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
				{	$affected = (new User($this->conn,'ezread'))->run([
						'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
						'license_id' => $this->license['id']
					]);
				}	else
				{	$affected = (new User($this->conn,'ezread'))->run([
						'field' => 'username',
						'value' => $this->mentions[0],
						'license_id' => $this->license['id']
					]);
				}
				if(!$affected) return;
				(new LicenseUser($this->conn,'edit',$affected['lu_id']))->run(['admin_id' => NULL]);
				$output = "I've demoted ".$this->formatMention($affected['first_name'],$affected['t_id']).'.';
			}
			if($output && !@$this->silent) $this->markMsg(['text' => $output]);
		}
		
		protected function extra()
		{	if(@$this->hashtags && @$this->update['message']['reply_to_message'])
			{	if((new Extra($this->conn,'match'))->run(['obj' => $this->hashtags]))
				{	if(strpos($this->command_trail,'mod') === 0)
					{	(new Extra($this->conn,'del'))->run(['code' => $this->hashtags[0]]);
						$this->command_trail = $this->hashtags[0];
						$this->extra();
					}	else 
					{	$output = '#'.$this->hashtags[0].' is already defined. /extra mod #'.$this->hashtags[0].' to set a new response.';
					}				
				}	else
				{	$chat = (new Chat($this->conn,'ezread'))->run(['value' => $this->update['message']['chat']['id']]);
					$message = (new Message($this->conn,'ezread'))->run([
						'value' => $chat['id'].'_'.$this->update['message']['reply_to_message']['message_id']
					]);
					if(!$message) return;
					$caption = @$message['media_id'] ? @$message['caption'] : $message['text'];
					$caption = explode($this->hashtags[0],$this->update['message']['text'])[1] ?: $caption;
					(new Extra($this->conn,'create'))->run([
						'code' => $this->hashtags[0],
						'media_id' => @$message['media_id'],
						'caption' => $caption
					]);
					$output = "I've set extra #".$this->hashtags[0].'.';
				}
			}	elseif(strpos($this->command_trail,'list') !== FALSE)
			{	$codes = (new Extra($this->conn,'_list'))->run();
				$output = '#'.implode(', #',$codes);
			}	elseif(strpos($this->command_trail,'del') !== FALSE)
			{	if((new Extra($this->conn,'del'))->run(['code' => $this->hashtags[0]])) $output = "I've deleted extra #".$this->hashtags[0].'.';
			}	elseif(!@$this->command_trail)
			{	$this->markMsg(['text' => '/extra #hashtag in reply to a message.']);
			}
			if(!@$this->silent && @$output) $this->markMsg(['text' => $output ]);
		}
		
		protected function trigger()
		{	if(!@$this->command_trail) return;
			$arr = explode(' ',$this->command_trail,2);
			if(strpos($this->command_trail,'del') === 0)
			{	if((new Extra($this->conn,'match'))->run(['obj' => [$arr[1]],'license_id'=>$this->license['id']]))
				{	(new Extra($this->conn,'del'))->run(['code' => $arr[1],'license_id'=>$this->license['id']]); 
					$output = "Trigger deleted for ".$arr[1].'.';
				}	else
				{	$output = $arr[1]." not found.";								
				}
			}	elseif(@$this->update['message']['reply_to_message'])
			{	if((new Extra($this->conn,'match'))->run(['obj' => [$arr[0]],'license_id'=>$this->license['id']]))
					(new Extra($this->conn,'del'))->run(['code' => $arr[0],'license_id'=>$this->license['id']]);
				$chat = (new Chat($this->conn,'ezread'))->run(['value' => $this->update['message']['chat']['id']]);
				$message = (new Message($this->conn,'ezread'))->run([
					'value' => $chat['id'].'_'.$this->update['message']['reply_to_message']['message_id']
				]);
				if(!$message) return;
				$caption = @$message['media_id'] ? @$message['caption'] : $message['text'];
				$caption = $arr[1] ?: $caption;
				if(!$message['media_id'] && !$caption) return;
				(new Extra($this->conn,'create'))->run([
					'code' => $arr[0],
					'media_id' => @$message['media_id'],
					'caption' => $caption,
					'license_id' => $this->license['id']
				]);
				$output = 'Trigger set for '.$arr[0].'.';
			
			}
			if(@$output && !@$this->silent) $this->markMsg(['text' => $output ]);
		}
		
		protected function badword() 
		{	if(strpos($this->command_trail,'list') !== FALSE)
			{	$badwords = (new Quip($this->conn,'all_active'))->run([
					'type' => 'badword',
					'license_id' => $this->license['id']				
				]);
				$output = 'Bad words include _'.implode('_, _',$badwords)."_.";
			}	elseif(strpos($this->command_trail,'del') !== FALSE)
			{	
			}	elseif(!@$this->command_trail)
			{	$output = 'example /badword shit';
			}	elseif(@$this->command_trail && count(explode(' ',$this->command_trail)) == 1)
			{	$chat = (new Chat($this->conn,'ezread'))->run(['value' => $this->update['message']['chat']['id']]);
				(new Quip($this->conn,'create'))->run([
					'type' => 'badword',
					'text' => trim(strtolower($this->command_trail)),
					'license_id' => $this->license['id']
				]);
				$output = ucfirst(trim(strtolower($this->command_trail))).' is already a bad word.';
			}	
			if(@$output && !@$this->silent) $this->markMsg(['text' => $output ]);
		}
		
		protected function badwords()
		{	$this->command_trail = "list";
			$this->badword();
		}
		
		protected function welcome()
		{	$text = (new Quip($this->conn,'active'))->run([
				'type' => 'welcome',
				'license_id'=>$this->license['id']
			]);
			$config = (new Config($this->conn,'ezread'))->run(['value' => 'welcome','license_id'=>$this->license['id']]);			
			if(!$this->command_trail && !$text)
			{	$output = '/welcome :name to :chat.';
			}	elseif(!$this->command_trail)
			{	$output = $text;
			}	elseif($this->command_trail == "off")
			{	$output = "Welcome off";
				(new Config($this->conn,'edit',$config['id']))->run(['enabled' => 0]);	
			}	elseif($this->command_trail == 'on')
			{	$output = "Welcome on";
				(new Config($this->conn,'edit',$config['id']))->run(['enabled' => 1]);	
			}	else
			{	if($text) (new Quip($this->conn,'deactivate'))->run([
					'type' => 'welcome',
					'license_id'=>$this->license['id']
				]);
				(new Quip($this->conn,'create'))->run([
					'type' => 'welcome',
					'text' => $this->command_trail,
					'license_id'=>$this->license['id']
				]);
				$output = "I've set the new welcome message.";
			}
			$this->markMsg(['text' => $output]);
		}

		protected function saywhat()
		{	$text = (new Quip($this->conn,'active'))->run([
				'type' => 'saywhat',
				'license_id'=>$this->license['id']
			]);
			if((!$this->command_trail && !$text) || strpos($this->command_trail,'help') === 0)
			{	$output = 'example /saywhat What you say mayo monkey ahh';
			}	elseif(!$this->command_trail)
			{	$output = $text;
			}	else
			{	if($text) (new Quip($this->conn,'deactivate'))->run([
					'type' => 'saywhat',
					'license_id'=>$this->license['id']
				]);
				(new Quip($this->conn,'create'))->run([
					'type' => 'saywhat',
					'text' => $this->command_trail,
					'license_id'=>$this->license['id']
				]);
				$output = "I've set the new response.";
			}
			$this->markMsg(['text' => $output]);
		}

		protected function reprimand()
		{	$text = (new Quip($this->conn,'all_active'))->run([
				'type' => 'reprimand',
				'license_id'=>$this->license['id']
			]);
			if((!$this->command_trail && !$text) || strpos($this->command_trail,'help') === 0)
			{	$output = '';
			}	elseif(!$this->command_trail)
			{	$output = implode("\n>\n",$text);
			}	else
			{	(new Quip($this->conn,'create'))->run([
					'type' => 'reprimand',
					'text' => $this->command_trail,
					'license_id'=>$this->license['id']
				]);
				$output = "I've set the new response.";
			}
			$this->markMsg(['text' => $output]);
		}
		
		protected function ban()
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
			) $output = "";
			else 
			{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
				{	$affected = (new User($this->conn,'ezread'))->run([
						'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
						'license_id' => $this->license['id']
					]);
				}	else
				{	$affected = (new User($this->conn,'ezread'))->run([
						'field' => 'username',
						'value' => $this->mentions[0],
						'license_id' => $this->license['id']
					]);
				}
				if(!$affected || @$affected['protection_id']) return;
				$mention = $this->formatMention(
					$affected['first_name'],
					$affected['t_id']
				);
				if(@$this->command_trail)
				{	$trail = explode(' ',$this->command_trail,2);
					if(is_numeric($trail[0]) && !@$this->silent)
					{	$delay = $trail[0] <= 30 ? $trail[0] : 30;
						$this->markMsg([
							'text' => "Ban incoming to $mention in $delay seconds. Say your goodbyes."
						]);
						if(@$trail[1]) $notes = $trail[1];
						sleep($delay);
					}	else
					{	$notes = $this->command_trail;
					}	
				}
				(new ReqOut('kickChatMember'))->run([
					'chat_id' => $this->update['message']['chat']['id'],
					'user_id' => $affected['t_id']
				]);
				$chat = (new Chat($this->conn,'ezid'))->run(['value'=> $this->update['message']['chat']['id']]);
				$message = $this->update['message']['reply_to_message']['message_id']
				?	(new Message($this->conn,'ezread'))->run([
						'value'=> $chat.'_'.$this->update['message']['reply_to_message']['message_id']
					])				
				:	(new Message($this->conn,'store'))->run([
						'obj' => $this->update['message'],
						'chat_id' => $chat,
						't_id' => $chat.'_'.$this->update['message']['message_id']
					]);
				(new Action($this->conn,'create'))->run([
					'type' => 'ban',
					'acting_user' => $this->db_from_user['id'],
					'affected_user' => $affected['id'],
					'notes' => $notes,				
					'chat_id' => $chat,
					'message_id' => $message['id']			
				]);
				$output = "I've banned $mention.";	
			}
			if (!@$this->silent) $this->markMsg(['text' => $output]);
		}
		
		protected function unban()
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
			) $output = "";
			else 
			{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
				{	$affected = (new User($this->conn,'ezread'))->run([
						'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
						'license_id' => $this->license['id']
					]);
				}	else
				{	$affected = (new User($this->conn,'ezread'))->run([
						'field' => 'username',
						'value' => $this->mentions[0],
						'license_id' => $this->license['id']
					]);
				}
				if(!$affected) return;
				(new ReqOut('unbanChatMember'))->run([
					'chat_id' => $this->update['message']['chat']['id'],
					'user_id' => $affected['t_id']
				]);
			}
		}

		protected function kick()
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
			) $output = "";
			else 
			{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
				{	$affected = (new User($this->conn,'ezread'))->run([
						'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
						'license_id' => $this->license['id']
					]);
				}	else
				{	$affected = (new User($this->conn,'ezread'))->run([
						'field' => 'username',
						'value' => $this->mentions[0],
						'license_id' => $this->license['id']
					]);
				}
				if(!$affected || @$affected['protection_id']) return;
				$mention = $this->formatMention(
					$affected['first_name'],
					$affected['t_id']
				);
				if(@$this->command_trail)
				{	$trail = explode(' ',$this->command_trail,2);
					if(is_numeric($trail[0]) && !@$this->silent)
					{	$delay = $trail[0] <= 30 ? $trail[0] : 30;
						$this->markMsg([
							'text' => "Kick incoming to $mention in $delay seconds."
						]);
						if(@$trail[1]) $notes = $trail[1]; 
						sleep($delay);
					}	else
					{	$notes = $this->command_trail;
					}	
				}
				(new ReqOut('kickChatMember'))->run([
					'chat_id' => $this->update['message']['chat']['id'],
					'user_id' => $affected['t_id']
				]);
				$this->unban();
				$chat = (new Chat($this->conn,'ezid'))->run([
					'value'=> $this->update['message']['chat']['id']
				]);
				$message = $this->update['message']['reply_to_message']['message_id']
				?	(new Message($this->conn,'ezread'))->run([
						'value'=> $chat.'_'.$this->update['message']['reply_to_message']['message_id']
					])				
				:	(new Message($this->conn,'store'))->run([
						'obj' => $this->update['message'],
						'chat_id' => $chat,
						't_id' => $chat.'_'.$this->update['message']['message_id']
					]);
				(new Action($this->conn,'create'))->run([
					'type' => 'kick',
					'acting_user' => $this->db_from_user['id'],
					'affected_user' => $affected['id'],
					'notes' => $notes,				
					'chat_id' => $chat,
					'message_id' => $message['id']			
				]);
				$output = "I've kicked $mention.";
			}
			if (!@$this->silent) $this->markMsg(['text' => $output]);
		}

		protected function timeout()
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
			) $output = "";
			else 
			{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
				{	$affected = (new User($this->conn,'ezread'))->run([
						'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
						'license_id' => $this->license['id']
					]);
				}	else
				{	$affected = (new User($this->conn,'ezread'))->run([
						'field' => 'username',
						'value' => $this->mentions[0],
						'license_id' => $this->license['id']
					]);
				}
				if(!$affected || @$affected['protection_id']) return;
				$until = (time())+3600;
				if(@$this->command_trail)
				{	$trail = explode(' ',$this->command_trail,2);
					$notes = is_numeric($trail[0]) ? $trail[1] : $this->command_trail;
				}
				$chat = (new Chat($this->conn,'ezid'))->run([
					'value'=> $this->update['message']['chat']['id']
				]);
				$message = $this->update['message']['reply_to_message']['message_id']
				?	(new Message($this->conn,'ezread'))->run([
						'value'=> $chat.'_'.$this->update['message']['reply_to_message']['message_id']
					])				
				:	(new Message($this->conn,'store'))->run([
						'obj' => $this->update['message'],
						'chat_id' => $chat,
						't_id' => $chat.'_'.$this->update['message']['message_id']
					]);
				$action = (new Action($this->conn,'create'))->run([
					'type' => 'timeout',
					'acting_user' => $this->db_from_user['id'],
					'affected_user' => $affected['id'],
					'notes' => @$notes,
					'until' => date("Y-m-d H:i:s",$until),				
					'chat_id' => $chat,
					'message_id' => $message['id']			
				]);
				$mention = $this->formatMention(
					$affected['first_name'],
					$affected['t_id']
				);
				if(@$this->command_trail)
				{	if(is_numeric($trail[0]) && !@$this->silent)
					{	$delay = $trail[0] <= 30 ? $trail[0] : 30;
						$announcement = $this->markMsg(['text' => "Timeout incoming to $mention in $delay seconds."]);
						sleep($delay);
					}
				}
				(new ReqOut('restrictChatMember'))->run([
					'chat_id' => $this->update['message']['chat']['id'],
					'user_id' => $affected['t_id'],
					'permissions' => [
						'can_send_messages' => FALSE
					],
					'until_date' => $until
				]);
//				if(!@$affected['og']) (new User($this->conn,'edit',$affected['id']))->run(['og' => NULL]);
				$this->updateMsg([
					'chat_id' => $announcement['result']['chat']['id'],
					'message_id' => $announcement['result']['message_id'],
					'text' => 'Timeout completed.',
					'no_keyboard' => 1				
				]);
				$output = "Timeout for $mention.";
				$callback_data = [
					'method' => 'reverse',
					'data' => [
						'id' => $action['id']
					]							
				];
				$keyboard = [[['text' => 'Reverse?', 'callback_data' => json_encode($callback_data)]]];	
			}
			if (!@$this->silent) $this->markMsg(['text' => $output, 'inline_keyboard' => $keyboard]);
		}
		
		protected function unmute()
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
			) $output = "";
			else 
			{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
				{	$affected = (new User($this->conn,'ezread'))->run([
						'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
						'license_id' => $this->license['id']
					]);
				}	else
				{	$affected = (new User($this->conn,'ezread'))->run([
						'field' => 'username',
						'value' => $this->mentions[0],
						'license_id' => $this->license['id']
					]);
				}
				if(!$affected) return;
				$chat = (new Chat($this->conn,'ezid'))->run([
					'value'=> $this->update['message']['chat']['id']
				]);
				$mutes = (new Action($this->conn,'active'))->run(['type' => 'timeout', 'affected_user' => $affected['id'], 'chat_id' => $chat]);
				$this->_untimeout(@$mutes[0]);
			}
			if(@$output and !@$this->silent) $this->markMsg(['text' => $output]);
		}

		protected function _untimeout($row = [])
		{	if($this->db_from_user['admin_id'] == 3 && !$row) return FALSE;
			if($row)
			{	(new Action($this->conn,'cancel'))->run([
					'type' => 'timeout',
					'affected_user' => $row['affected_user'],
					'chat_id' => $row['chat_id']
				]);
			}
			if(!@$row['affected_user'])
			{	if($this->update['message']['reply_to_message'] || $this->text_mentions)
				{	$user_id =  (new User($this->conn,'ezid'))->run([
						'value' => $this->update['message']['reply_to_message']['from']['id'] ?: $this->text_mentions[0]['id']
					]);
				}	elseif($this->mentions)
				{	$user_id =  (new User($this->conn,'ezid'))->run([
						'field' => 'username',						
						'value' => $this->mentions[0]
					]);
				}
			}	else
			{	$user_id = $row['affected_user'];
			}

			$affected = (new User($this->conn,'read',$user_id))->run(['license_id' => $this->license['id']]);
			if(!$affected) return;
			$mention = $this->formatMention(
				$affected['first_name'].' '.$affected['last_name'],
				$affected['t_id']			
			);
			$output = "$mention released by ".$this->db_from_user['first_name'].' '.$this->db_from_user['last_name'].'.';
			if(!@$this->silent) $this->markMsg(['text' => $output]);
			$og_on = (new Config($this->conn,'ezread'))->run(['value' => 'og mode','license_id'=>$this->license['id']])['enabled'];			
			$perms = $this->setPerms(/*!$og_on ? 1 : $affected['og']*/);
			(new ReqOut('restrictChatMember'))->run([
				'chat_id' => $row ? (new Chat($this->conn,'read',$row['chat_id']))->run()['t_id'] : $this->update['message']['chat']['id'],
				'user_id' => $affected['t_id'],
				'permissions' => $perms				
			]);
//			if(!$affected['og'] && $og_on) (new User($this->conn,'edit',$affected['id']))->run(['og' => 0]);
		}

		protected function gag($data = NULL)
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
				 && !@$data['affected']
			) $output = "";
			else 
			{	if(!@$data['affected']) {
					if($this->update['message']['reply_to_message']  || @$this->text_mentions)
					{	$affected = (new User($this->conn,'ezread'))->run([
							'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
							'license_id' => $this->license['id']
						]);
					}	else
					{	$affected = (new User($this->conn,'ezread'))->run([
							'field' => 'username',
							'value' => $this->mentions[0],
							'license_id' => $this->license['id']
						]);
					}
					if(!$affected) {
						$affected = (new User($this->conn,'store'))->run([
							'obj'=> $this->update['message']['reply_to_message']['from'],
							'license_id' => $this->license["id"]
						]);
					}	
				}	else
				{	$affected = $data['affected'];
				}
				if(!$affected || @$affected['protection_id']) return;
				if(@$this->command_trail || $data['duration'])
				{	$trail = explode(' ',$this->command_trail,3);
					if(is_numeric($trail[0]) || $data['duration'])
					{	$date = new DateTime();
						$date->setTimestamp(time());
						$date_string = $data['duration'] ?: $trail[0].' '.$trail[1];
						$interval = DateInterval::createFromDateString($date_string);
						if(!$interval)
						{	$notes = $this->command_trail;
						}	else
						{	$date->add($interval);
							$until = $date->getTimestamp();
							$year = new DateTime();
							$year->setTimestamp(time());
							$interval = DateInterval::createFromDateString("1 year");
							$year->add($interval);
							if($date > $year) $until = $year->getTimestamp();
							if(@$trail[2]) $notes = $trail[2];
						}
					}	else
					{	$notes = $this->command_trail;
					}	
				}
				if($this->db_from_user['admin_id'] == 3)
				{	$day = new DateTime();
					$day->setTimestamp(time());
					$interval = DateInterval::createFromDateString("24 hours");
					$day->add($interval);
					$short = new DateTime();
					$short->setTimestamp(time());
					$interval = DateInterval::createFromDateString("1 minute");
					$short->add($interval);
					if(!$date || $date > $day || $date < $short) $until = $day->getTimestamp();
				}
				(new ReqOut('restrictChatMember'))->run([
					'chat_id' => $this->chat['t_id'],
					'user_id' => $affected['t_id'],
					'permissions' => [
						'can_send_messages' => FALSE
					],
					'until_date' => $until
				]);
				$chat = (new Chat($this->conn,'ezid'))->run([
					'value'=> $this->update['message']['chat']['id']
				]);
				$message = $this->update['message']['reply_to_message']['message_id']
				?	(new Message($this->conn,'ezread'))->run([
						'value'=> $chat.'_'.$this->update['message']['reply_to_message']['message_id']
					])				
				:	(new Message($this->conn,'store'))->run([
						'obj' => $this->update['message'],
						'chat_id' => $chat,
						't_id' => $chat.'_'.$this->update['message']['message_id']
					]);
				$action = (new Action($this->conn,'create'))->run([
					'type' => 'gag',
					'acting_user' => $this->db_from_user['id'],
					'affected_user' => $affected['id'],
					'notes' => $notes,
					'until' => date("Y-m-d H:i:s",$until),
					'chat_id' => $chat,
					'message_id' => $message['id']			
				]);
				$output = "I've gagged ".$this->formatMention($affected['first_name'],$affected['t_id']).'.';	
				$callback_data = [
					'method' => 'reverse',
					'data' => [
						'id' => $action['id']
					]							
				];
				$keyboard = [[['text' => 'Reverse?', 'callback_data' => json_encode($callback_data)]]];	
			}
			if (!@$this->silent) $this->markMsg(['text' => $output, 'inline_keyboard' => $keyboard]);
//			if(!$affected['og']) (new User($this->conn,'edit',$affected['id']))->run(['og' => NULL]);
		}

		protected function ungag()
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
			) $output = "";
			else 
			{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
				{	$affected = (new User($this->conn,'ezread'))->run([
						'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
						'license_id' => $this->license['id']
					]);
				}	else
				{	$affected = (new User($this->conn,'ezread'))->run([
						'field' => 'username',
						'value' => $this->mentions[0],
						'license_id' => $this->license['id']
					]);
				}
				if(!$affected) return;
				$chat = (new Chat($this->conn,'ezid'))->run([
					'value'=> $this->update['message']['chat']['id']
				]);
				$gags = (new Action($this->conn,'active'))->run(['type' => 'gag', 'affected_user' => $affected['id'], 'chat_id' => $chat]);
				$this->_ungag(@$gags[0]);
			}
			if(@$output and !@$this->silent) $this->markMsg(['text' => $output]);
		}

		protected function _ungag($row)
		{	if($this->db_from_user['admin_id'] == 3 && (!$row  || $this->db_from_user['id'] != $row['acting_user'])) return FALSE; 
			if($row)
			{	(new Action($this->conn,'cancel'))->run([
					'type' => 'gag',
					'affected_user' => $row['affected_user'],
					'chat_id' => $row['chat_id']
				]);
			}
			if(!@$row['affected_user'])
			{	if($this->update['message']['reply_to_message'] || $this->text_mentions)
				{	$user_id =  (new User($this->conn,'ezid'))->run([
						'value' => $this->update['message']['reply_to_message']['from']['id'] ?: $this->text_mentions[0]['id']
					]);
				}	elseif($this->mentions)
				{	$user_id =  (new User($this->conn,'ezid'))->run([
						'field' => 'username',						
						'value' => $this->mentions[0]
					]);
				}
			}	else
			{	$user_id = $row['affected_user'];
			}
			$affected = (new User($this->conn,'read',$user_id))->run(['license_id' => $this->license['id']]);
			if(!$affected) return;
			$mention = $this->formatMention(
				$affected['first_name'].' '.$affected['last_name'],
				$affected['t_id']			
			);
			$output = "$mention ungagged by ".$this->db_from_user['first_name'].' '.$this->db_from_user['last_name'].'.';
			if(!@$this->silent) $this->markMsg(['text' => $output]);			
			$og_on = (new Config($this->conn,'ezread'))->run(['value' => 'og mode','license_id'=>$this->license['id']])['enabled'];			
			$perms = $this->setPerms(/*!$og_on ? 1 : $affected['og']*/);
			(new ReqOut('restrictChatMember'))->run([
				'chat_id' => $row ? (new Chat($this->conn,'read',$row['chat_id']))->run()['t_id'] : $this->update['message']['chat']['id'],
				'user_id' => $affected['t_id'],
				'permissions' => $perms				
			]);
//			if(!$affected['og'] && $og_on) (new User($this->conn,'edit',$affected['id']))->run(['og' => 0]);
		}
		
		protected function warn($data = [])
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
				 && !$data['affected']
			) $output = "";
			else 
			{	if(!@$data['affected'])
				{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
					{	$affected = (new User($this->conn,'ezread'))->run([
							'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
							'license_id' => $this->license['id']
						]);
					}	else
					{	$affected = (new User($this->conn,'ezread'))->run([
							'field' => 'username',
							'value' => $this->mentions[0],
							'license_id' => $this->license['id']
						]);
					}
				}	else
				{	$affected = $data['affected']; 
				}
				if(!$affected || @$affected['protection_id']) return;
				$chat = (new Chat($this->conn,'ezid'))->run([
					'value'=> $this->update['message']['chat']['id']
				]);
				$message = $this->update['message']['reply_to_message']['message_id']
				?	(new Message($this->conn,'ezread'))->run([
						'value'=> $chat.'_'.$this->update['message']['reply_to_message']['message_id']
					])				
				:	(new Message($this->conn,'store'))->run([
						'obj' => $this->update['message'],
						'chat_id' => $chat,
						't_id' => $chat.'_'.$this->update['message']['message_id']
					]);
				$action = (new Action($this->conn,'create'))->run([
					'type' => 'warn',
					'acting_user' => NULL,
					'affected_user' => $affected['id'],
					'notes' => @$data['note'] ?: @$this->command_trail,				
					'chat_id' => $chat,
					'message_id' => $message['id']			
				]);
				$active = count((new Action($this->conn,'active'))->run(['type' => 'warn', 'affected_user' => $affected['id'], 'chat_id' => $chat]));
				$mention = $this->formatMention(
					$affected['first_name'],
					$affected['t_id']
				);
				$output = "$mention has been warned ($active of 3) and received one demerit.";
				$callback_data = [
					'method' => 'reverse',
					'data' => [
						'id' => $action['id']
					]							
				];
				$keyboard = [[['text' => 'Reverse?', 'callback_data' => json_encode($callback_data)]]];	
			}
			if (!@$this->silent) $this->markMsg(['text' => $output, 'inline_keyboard' => $keyboard ]);
			if($active >= 3)
			{	$this->gag(['affected' => $affected, 'duration' => '1 hour']);
				(new Action($this->conn,'cancel'))->run(['type' => 'warn', 'affected_user' => $affected['id'], 'chat_id' => $chat]);
			}
			$this->silent = TRUE;
			$this->demerit(['affected' => $affected]);
		}
		
		protected function _unwarn($warn)
		{	(new Action($this->conn,'edit',$warn['id']))->run(['cancelled' => 1]);
			$active = count((new Action($this->conn,'active'))->run([
				'type' => 'warn',
				'affected_user' => $warn['affected_user'],
				'chat_id' => $warn['chat_id']
			]));
			$affected = (new User($this->conn,'read',$warn['affected_user']))->run(['license_id' => $this->license['id']]);
			$output = "Warn removed from ".$affected['first_name'];
			$output .= ' by '.$this->db_from_user['first_name'].' '.$this->db_from_user['last_name'].'. ';
			$output .= "($active of 3)";
			return ['text' => $output];
		}
		
		protected function nowarns()
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
			) $output = "";
			else 
			{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
				{	$affected = (new User($this->conn,'ezread'))->run([
						'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
						'license_id' => $this->license['id']
					]);
				}	else
				{	$affected = (new User($this->conn,'ezread'))->run([
						'field' => 'username',
						'value' => $this->mentions[0],
						'license_id' => $this->license['id']
					]);
				}
				if(!$affected) return;
				$chat = (new Chat($this->conn,'ezid'))->run([
					'value'=> $this->update['message']['chat']['id']
				]);
				$active = count((new Action($this->conn,'active'))->run(['type' => 'warn', 'affected_user' => $affected['id'], 'chat_id' => $chat]));
				(new Action($this->conn,'cancel'))->run(['type' => 'warn', 'affected_user' => $affected['id'], 'chat_id' => $chat]);
				$output = "I've removed all $active warnings from ".$this->formatMention($affected['first_name'],$affected['t_id']);
				$output .= " as directed by ".$this->db_from_user['first_name'].' '.$this->db_from_user['last_name'].'. ';
			}
			if(!$this->silent) $this->markMsg(['text' => $output]);
		}
		
		protected function reverse()
		{	$action = (new Action($this->conn,'read',$this->cQuery['data']['id']))->run([]);
			if($action['cancelled'])
			{	$this->answerQuery(['text' => 'Already cancelled.']);
			}	else 
			{	if(FALSE !== $result = $this->{'_un'.$action['type']}($action) )
				{	$this->answerQuery(['text' => 'OK']);			
					$this->updateMsg([
						'chat_id' => $this->update['callback_query']['message']['chat']['id'],
						'message_id' => $this->update['callback_query']['message']['message_id'],
						'text' => $result['text'] 
							?: ucfirst($action['type']).' reversed by '.$this->db_from_user['first_name'].' '.$this->db_from_user['last_name'].'.',
						'no_keyboard' => 1			
					]);
				}	else
				{	$this->answerQuery(['text' => 'Nosimping.']);
				}
			}
		}
		
		protected function history()
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
			) $this->markMsg(['text' => ""]);
			else 
			{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
				{	$affected = (new User($this->conn,'ezread'))->run([
						'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
						'license_id' => $this->license['id']
					]);
				}	else
				{	$affected = (new User($this->conn,'ezread'))->run([
						'field' => 'username',
						'value' => $this->mentions[0],
						'license_id' => $this->license['id']
					]);
				}
				if(!$affected) return;
				$text = "History for ";
				$text .= $affected['first_name'].' ';
				$text .= $affected['last_name'].":";
				if(FALSE !== strpos($this->command_trail,'sum'))
				{	$history = (new User($this->conn,'summary',$affected['id']))->run();
					$text .= " (summary)\n\n";				
					foreach ($history as $key => $val)
					{	$text .= "$key: $val";
						$text .= "\n";
					}
				}	else
				{	$short = (FALSE !== strpos($this->command_trail,'short')) ? 1 : 0;
					$history = (new User($this->conn,'history',$affected['id']))->run(['limit' => $short]);
					$text .= $short ? " (last 10)\n\n" : " (full)\n\n";
					foreach ($history as $row)
					{	$text .= '['.date('m/d/Y',strtotime($row['timestamp']))."](https://t.me/c/".substr($row['chat'],4)."/".explode('_',$row['message'])[1]."): ";
						$text .= $row['type']." ";
						$text .= "from ".($row['acting_user'] ? $row['acting_first'].' '.$row['acting_last'] : 'bot');
						if($row['notes']) $text .= " for ".$row['notes'];
						$text .= "\n";
					}
				}
				$this->markMsg(['text' => $text, 'chat_id' => $this->db_from_user['t_id'], 'no_force_italic'=>1]);
				if(!$this->silent) $this->quickRepMsg(['text' => 'Check DM.']);
			}
		}

		protected function cry()
		{	$this->markMsg(['text' => '*'.$this->command_trail.'*', 'chat_id' => -1001845303406, 'no_force_italic'=>1]);
		}
		
		protected function delete()
		{	(new ReqOut('deleteMessage'))->run([
				'chat_id' => $this->update['message']['chat']['id'],
				'message_id' => $this->update['message']['message_id']
			]);
			if(@$this->update['message']['reply_to_message'])
			{	(new ReqOut('deleteMessage'))->run([
					'chat_id' => $this->update['message']['chat']['id'],
					'message_id' => $this->update['message']['reply_to_message']['message_id']
				]);
			}
		}
		
		protected function merit()
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
			) $this->markMsg(['text' => ""]);
			else 
			{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
				{	$affected = (new User($this->conn,'ezread'))->run([
						'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
						'license_id' => $this->license['id']
					]);
				}	else
				{	$affected = (new User($this->conn,'ezread'))->run([
						'field' => 'username',
						'value' => $this->mentions[0],
						'license_id' => $this->license['id']
					]);
				}
				if(!$affected) return;
				$result = (new League($this->conn,'create'))->run([
					'user_id' => $affected['id'],
					'acting_id' => $this->db_from_user['id'],
					'license_id' => $this->license['id']				
				]);
				$mention = $this->formatMention($affected['first_name'],$affected['t_id']);
				if ($result) $output = "One merit to $mention.";
				if(!$this->silent && $output) $this->markMsg(['text' => $output ]);
			}
		}

		protected function demerit($data = [])
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
				 && !@$data['affected']
			) $output = "";
			else 
			{	if(!@$data['affected'])
				{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
					{	$affected = (new User($this->conn,'ezread'))->run([
							'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
						'license_id' => $this->license['id']
						]);
					}	else
					{	$affected = (new User($this->conn,'ezread'))->run([
							'field' => 'username',
							'value' => $this->mentions[0],
						'license_id' => $this->license['id']
						]);
					}
					if(!$affected || @$affected['protection_id']) return;
				}	else
				{	$affected = $data['affected'];
				}
				$result = (new League($this->conn,'create'))->run([
					'user_id' => $affected['id'],
					'value' => -1,
					'acting_id' => $this->db_from_user['id'],
					'license_id' => $this->license['id']				
				]);
				$mention = $this->formatMention($affected['first_name'],$affected['t_id']);
				if ($result) $output = "One demerit to $mention.";
				if(!$this->silent && $output) $this->markMsg(['text' => $output ]);
			}
		}
		
		protected function league()
		{	if(strpos($this->command_trail,'icon') !== FALSE)
			{	(new Quip($this->conn,'deactivate'))->run([
					'type' => 'leagueicon',
					'license_id'=>$this->license['id']
				]);
				$new = trim(explode('icon',$this->command_trail)[1]);
				(new Quip($this->conn,'create'))->run([
					'type' => 'leagueicon',
					'text' => $new,
					'license_id'=>$this->license['id']
				]);
				$output = "I've set the league icon.";
			}	else
			{	$method = strpos($this->command_trail,'full') === FALSE ? 'topsnbottoms' : 'sum';
				$data = (new League($this->conn,$method))->run(['license_id'=>$this->license['id']]);
//				$icon = (new Quip($this->conn,'active'))->run(['type' => 'leagueicon','license_id'=>$this->license['id']]);
				if(!$data) return;
				$output = "*Fag Chat Merit Points League Table:*\n\n_";
				if($method == 'sum')
				{	foreach($data as $row)
					{	$name = $row['first_name'] ? $row['first_name'].' ' : '';
						$name .= $row['last_name'];
						$name = trim(str_replace('_','\_',$name));
						$output .= $this->formatMention($name,$row['t_id']).': '.$row['sum']."\n"; 
					}
				}	else 
				{	foreach($data[0] as $row)
					{	$name = $row['first_name'] ? $row['first_name'].' ' : '';
						$name .= $row['last_name'];
						$name = trim(str_replace('_','\_',$name));
						$output .= $this->formatMention($name,$row['t_id']).': '.$row['sum']."\n"; 
					}
					$output .= "...\n";
					foreach($data[1] as $row)
					{	$name = $row['first_name'] ? $row['first_name'].' ' : '';
						$name .= $row['last_name'];
						$name = trim(str_replace('_','\_',$name));
						$output .= $this->formatMention($name,$row['t_id']).': '.$row['sum']."\n"; 
					}
				}
			}
			$this->markMsg(['text' => $output.'_','no_force_italic'=>1]);
		}
		
		protected function spam()
		{	if(!$this->update['message']['reply_to_message']) return FALSE;
			if(!$this->update['message']['reply_to_message']['entities']
				&& !$this->update['message']['reply_to_message']['caption_entities']) return FALSE;
			foreach(@$this->update['message']['reply_to_message']['entities'] as $row)
			{	switch($row['type'])
				{	case 'mention':
						$t = mb_convert_encoding($this->update['message']['reply_to_message']['text'],"UTF-16","UTF-8");
						$arr[] = ltrim(mb_convert_encoding(substr($t,$row['offset']*2,$row['length']*2),"UTF-8","UTF-16"),'@');
					break;
					case 'url':
						$t = mb_convert_encoding($this->update['message']['reply_to_message']['text'],"UTF-16","UTF-8");						
						$arr[] = mb_convert_encoding(substr($t,$row['offset']*2,$row['length']*2),"UTF-8","UTF-16");
					break;
					case 'text_link':
						$arr[] = $row['url'];
						$t = mb_convert_encoding($this->update['message']['reply_to_message']['text'],"UTF-16","UTF-8");						
						$arr[] = mb_convert_encoding(substr($t,$row['offset']*2,$row['length']*2),"UTF-8","UTF-16");
					break;
				}
			}
			foreach(@$this->update['message']['reply_to_message']['caption_entities'] as $row)
			{	switch($row['type'])
				{	case 'mention':
						$t = mb_convert_encoding($this->update['message']['reply_to_message']['caption'],"UTF-16","UTF-8");
						$arr[] = ltrim(mb_convert_encoding(substr($t,$row['offset']*2,$row['length']*2),"UTF-8","UTF-16"),'@');
					break;
					case 'url':
						$t = mb_convert_encoding($this->update['message']['reply_to_message']['caption'],"UTF-16","UTF-8");
						$arr[] = mb_convert_encoding(substr($t,$row['offset']*2,$row['length']*2),"UTF-8","UTF-16");
					break;
					case 'text_link':
						$arr[] = $row['url'];
						$t = mb_convert_encoding($this->update['message']['reply_to_message']['caption'],"UTF-16","UTF-8");
						$arr[] = mb_convert_encoding(substr($t,$row['offset']*2,$row['length']*2),"UTF-8","UTF-16");
					break;
				}
			}
			if(!@$arr) return FALSE;
			$this->delete();
			foreach ($arr as $link)
			{	(new Spam($this->conn,'create'))->run(["link" => $link]);
			}
			$this->silent = TRUE;
			$this->command_trail = "spam";
			$this->gag(); 	
		}
		
		protected function repent()
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
			) $output = "";
			else
			{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
				{	$affected = (new User($this->conn,'ezread'))->run([
						'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
						'license_id' => $this->license['id']
					]);
				}	else
				{	$affected = (new User($this->conn,'ezread'))->run([
						'field' => 'username',
						'value' => $this->mentions[0],
						'license_id' => $this->license['id']
					]);
				}
				if(!$affected) return;
				$mention = $this->formatMention($affected['first_name'],$affected['t_id']);
				if(@$affected['unrepentant'])
				{	
				}	else
				{	(new LicenseUser($this->conn,'edit',$affected['lu_id']))->run(['unrepentant' => 1]);
				}
				$output = "$mention must repent.";
			}
			if(@$output && !@$this->silent) $this->markMsg(['text' => $output]);
		}

		protected function unrepent()
		{	if(!@$this->update['message']['reply_to_message']
				 && !@$this->mentions
				 && !@$this->text_mentions
			) $output = "";
			else
			{	if($this->update['message']['reply_to_message']  || @$this->text_mentions)
				{	$affected = (new User($this->conn,'ezread'))->run([
						'value' => @$this->text_mentions[0]['id'] ?: $this->update['message']['reply_to_message']['from']['id'],
						'license_id' => $this->license['id']
					]);
				}	else
				{	$affected = (new User($this->conn,'ezread'))->run([
						'field' => 'username',
						'value' => $this->mentions[0],
						'license_id' => $this->license['id']
					]);
				}
				if(!$affected) return;
				$mention = $this->formatMention($affected['first_name'],$affected['t_id']);
				if(!@$affected['unrepentant'])
				{	
				}	else
				{	(new LicenseUser($this->conn,'edit',$affected['lu_id']))->run(['unrepentant' => 0]);
				}
				$output = "$mention has repented.";
			}
			if(@$output && !@$this->silent) $this->markMsg(['text' => $output]);
		}
		
//		alias methods

		protected function mute()
		{	$this->gag();
		}
		protected function deport()
		{	$this->kick();
		}
		protected function excommunicate()
		{	$this->ban();
		}
		protected function skinnyfat()
		{	$this->timeout();
		}
		protected function parler()
		{	$this->warn();
		}
		protected function chattel()
		{	$this->fem();
		}
		protected function table()
		{	$this->league();
		}
	}
?>