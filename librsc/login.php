<?php
if(!login())
{	die("I DON'T KNOW YOU");	
}

function login()
{	if(!$_REQUEST) return FALSE;
	$arr = $_REQUEST;
	if(!$arr['hash']) return FALSE;
	$req_hash = $arr['hash'];
	unset($arr['hash']);
	ksort($arr);
	foreach($arr as $key => $value)
	{	if($key == 'hash') continue;
		$cs[] = $key.'='.$value;
	}
	if(strcmp($req_hash,hash_hmac('sha256',implode('\n',$cs),hash('sha256',auth,TRUE))))
	{	session_regenerate_id(TRUE);
		$_SESSION['user'] = $arr;
		header("Location: /bot/");
		exit();
	}
	return true;
}