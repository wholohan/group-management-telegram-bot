<?php
	$req = json_decode(file_get_contents("php://input"),TRUE);
	if(!$req['classname'] 
		|| !$req['method'] 
		|| !$_SESSION['user'] 
		|| 0/*!(new User($conn,'ezread'))->run(['field' => 't_id', 'value' => $_SESSION['user']['id']])['can_ui']*/
		)
	{	$ret = [ 'ok' => 0, 'error' => 'Ill-begotten request.'];
	}	else
	{	$inst = new $req['classname']($conn,$req['method'],@$req['data']['id']);
		$data = $inst->run(@$req['data']);
		if($inst->err_code)	$ret = [ 'ok' => 0, 'error' => $inst->err_code, 'data' => NULL ]; 
		else	$ret = [ 'ok' => 1, 'error' => NULL, 'data' => $data ?: NULL ]; 
	}
	header("Content-type: application/json");
	echo json_encode($ret);