<?php
	session_start();
	ini_set('display_errors','on');
	include $_SERVER['DOCUMENT_ROOT'].'/bot/librsc/auth.php'; // API and PDO credentials
	include $_SERVER['DOCUMENT_ROOT'].'/bot/librsc/pdo.php'; // defines custom MyPDO wrapper class, creates connection
	include $_SERVER['DOCUMENT_ROOT'].'/bot/librsc/storage.php'; // class for SQL operations
	include $_SERVER['DOCUMENT_ROOT'].'/bot/librsc/hookin.php'; // class for processing updates from Telegram
	include $_SERVER['DOCUMENT_ROOT'].'/bot/librsc/reqout.php'; // class to query Telegram Bot API

	if($_GET['id'] == auth) // authorized request from Telegram
	{	set_time_limit(0);
		ob_start();
		echo '1';
		http_response_code(200);
		header('Content-Type: text/html; charset=UTF-8');
		header('Content-Length: '.ob_get_length());
		ob_end_flush();
		ob_flush();
		flush();
		session_write_close();
		(new HookIn($conn))->run(); 
	}	elseif($_GET['ajax'] == 1)	// ajax request from a browser
	{	include $_SERVER['DOCUMENT_ROOT'].'/bot/librsc/ajax.php';
	}	else // web request
	{	if(!$_SESSION['user'])
		{	include $_SERVER['DOCUMENT_ROOT'].'/bot/librsc/login.php';
		}	else
		{	include $_SERVER['DOCUMENT_ROOT'].'/bot/librsc/admin_panel.php';
		}
	}
?>
